package com.alexgrig.homework15.humans;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class HumanTest {

    @Test
    public void IsToStringReturnsCorrectString() {

        Human newWoman = new Woman("Olena", "Smith", 1980);
        String result = newWoman.toString();
        assertThat(result).isEqualTo("{Olena Smith, birthDate=01/01/1980, IQLevel=0, schedule=null}");
    }
}
