package com.alexgrig.homework15;


import com.alexgrig.homework15.humans.Human;
import com.alexgrig.homework15.humans.Man;
import com.alexgrig.homework15.humans.Woman;
import com.alexgrig.homework15.pets.Dog;
import com.alexgrig.homework15.pets.DomesticCat;
import com.alexgrig.homework15.pets.Pet;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

//import static org.junit.Assert.*;

public class FamilyServiceTest {
    private static Human mam1;
    private static Human dad1;
    private static Human mam2;
    private static Human dad2;
    private static Human child1;
    private static Human child2;
    private static Family family1;
    private static Family family2;
    private static CollectionFamilyDao newCollection;
    private static FamilyService familyService;


    @Before
    public void executedBeforeEach() {
        mam1 = new Woman("Ann", "Smith", 1952);
        mam2 = new Woman("Mary", "MacCoin", 1975);
        dad1 = new Man("Bill", "Smith", 1950);
        dad2 = new Man("Charles", "MacBit", 1972);
        family1 = new Family(mam1, dad1);
        family2 = new Family(mam2, dad2);
        newCollection = new CollectionFamilyDao(family1, family2);
        familyService = new FamilyService(newCollection);
        child1 = new Man("Garry", "Smith", 2000);
        child2 = new Woman("Isaura", "Slave", 2003);
        family1.addChild(child1);
    }


    @Test
    public void isGetFamiliesBiggerThan_returnCorrectFamilyList() {
        //given
        //when
        List<Family> result = familyService.getFamiliesBiggerThan(2);

        //then
        assertThat(result).contains(family1).doesNotContain(family2);
    }

    @Test
    public void isGetFamiliesLessThan_returnCorrectFamilyList() {
        //given
        //when
        List<Family> result = familyService.getFamiliesLessThan(3);

        //then
        assertThat(result).contains(family2).doesNotContain(family1);
    }

    @Test
    public void isCountFamiliesWithMemberNumber_returnCorrectNumber() {
        //given
        //when
        int result = familyService.countFamiliesWithMemberNumber(3);
        //then
        assertThat(result).isEqualTo(1);
    }

    @Test
    public void isCreateNewFamily_returnNull_whenFirstArgumentIsNull() {
        //given
        //when
        Family result = familyService.createNewFamily(null, child1);
        //then
        assertThat(result).isNull();
    }

    @Test
    public void isCreateNewFamily_returnNull_whenSecondArgumentIsNull() {
        //given
        //when
        Family result = familyService.createNewFamily(child1, null);
        //then
        assertThat(result).isNull();
    }

    @Test
    public void isCreateNewFamily_returnNull_whenBothArgumentAreNull() {
        //given
        //when
        Family result = familyService.createNewFamily(null, null);
        //then
        assertThat(result).isNull();
    }

    @Test
    public void isCreateNewFamily_doesNotChangeDatabase_whenReturnsNull() {
        //given
        //when
        List<Family> before = new ArrayList<>(newCollection.getAllFamilies());
        Family family3 = familyService.createNewFamily(null, null);
        //then
        List<Family> after = new ArrayList<>(newCollection.getAllFamilies());

        assertThat(before).isEqualTo(after);
    }

    @Test
    public void isCreateNewFamily_addNewFamilyToDatabase_AndRemoveOldFamilyFromDatabase() {
        //given
        //when
        Family family3 = familyService.createNewFamily(child2, dad2);
        //then
        List<Family> result = newCollection.getAllFamilies();
        assertThat(result).contains(family1, family3).doesNotContain(family2);
    }

    @Test
    public void isCreateNewFamily_returnsCorrectFamily() {
        //given
        //when
        Family expected = new Family(child2, child1);
        Family result = familyService.createNewFamily(child2, child1);
        //then
        assertThat(result).isEqualTo(expected);
    }


    @Test
    public void isCreateNewFamily_putsWomanToMotherAndManToFather_whenWomanFirstAndManSecondArgument() {
        //given
        //when
        Family result = familyService.createNewFamily(child2, child1);
        //then
        assertThat(result.getMother()).isEqualTo(child2);
        assertThat(result.getFather()).isEqualTo(child1);
    }

    @Test
    public void isCreateNewFamily_putsWomanToMotherAndManToFather_whenWomanSecondAndManFirstArgument() {
        //given
        //when
        Family result = familyService.createNewFamily(child1, child2);
        //then
        assertThat(result.getMother()).isEqualTo(child2);
        assertThat(result.getFather()).isEqualTo(child1);
    }

    @Test
    public void isBornChild_returnNull_whenFamilyIsNull() {
        //given
        //when
        Family result = familyService.bornChild(null, "Bob", "Mag");
        //then
        assertThat(result).isNull();
    }

    @Test
    public void isBornChild_returnInitialFamilyWithoutAddedChildren_whenOneNameIsNull() {
        //given
        //when
        Family initial = new Family(mam2, dad2);
        Family result1 = familyService.bornChild(family2, null, "Mag");
        Family result2 = familyService.bornChild(family2, "Bob", null);
        //then
        assertThat(result1.getChilds()).isEqualTo(initial.getChilds());
        assertThat(result2.getChilds()).isEqualTo(initial.getChilds());

    }

    @Test
    public void isBornChild_returnInitialFamilyWithoutAddedChildren_whenNameIsIncorrect() {
        //given
        //when
        Family initial = new Family(mam2, dad2);
        Family result1 = familyService.bornChild(family2, "Bob2", "Mag");
        Family result2 = familyService.bornChild(family2, "Bob", "M");
        //then
        assertThat(result1.getChilds()).isEqualTo(initial.getChilds());
        assertThat(result2.getChilds()).isEqualTo(initial.getChilds());
    }

    @Test
    public void isBornChild_addsChildToFamily() {
        //given
        //when
        familyService.bornChild(family2, "Bob", "Mag");
        //then
        assertThat(family2.getChilds().size()).isEqualTo(1);
    }

    @Test
    public void isBornChild_updateFamilyInDatabase() {
        //given
        //when
        List<Human> initial = new ArrayList<>(newCollection.getFamilyByIndex(1).getChilds());
        familyService.bornChild(family2, "Bob", "Mag");
        List<Human> result = newCollection.getFamilyByIndex(1).getChilds();
        //then
        assertThat(result).isNotEqualTo(initial);
    }

    @Test
    public void isAdoptChild_returnNull_whenFamilyIsNull() {
        //given
        //when
        Family result = familyService.adoptChild(null, child2);
        //then
        assertThat(result).isNull();
    }

    @Test
    public void isAdoptChild_returnInitialFamilyWithoutAddedChildren_whenChildNameIsNull() {
        //given
        //when
        Family initial = new Family(mam2, dad2);
        Family result1 = familyService.adoptChild(family2, null);
        //then
        assertThat(result1.getChilds()).isEqualTo(initial.getChilds());
    }

    @Test
    public void isAdoptChild_addsChildToFamily() {
        //given
        //when
        familyService.adoptChild(family2, child2);
        //then
        assertThat(family2.getChilds().get(0)).isEqualTo(child2);
    }

    @Test
    public void isAdoptChild_updateFamilyInDatabase() {
        //given
        //when
        List<Human> initial = new ArrayList<>(newCollection.getFamilyByIndex(1).getChilds());
        familyService.adoptChild(family2, child2);
        List<Human> result = newCollection.getFamilyByIndex(1).getChilds();
        //then
        assertThat(result).isNotEqualTo(initial);
    }

    @Test
    public void isDeleteAllChildrenOlderThen_deletesCorrectChildren() {
        //given
        family1.addChild(child2);
        newCollection.saveFamily(family1);
        //when
        familyService.deleteAllChildrenOlderThen(18);
        List<Human> expected = new ArrayList<>(Arrays.asList(child2));
        List<Human> result = familyService.getAllFamilies().get(0).getChilds();
        //then
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void isCount_returnCorrectNumber() {
        //given
        //when
        int result = familyService.count();
        //then
        assertThat(result).isEqualTo(2);
    }

    @Test
    public void isGetPets_returnCorrectSet() {
        //given
        Pet pet1 = new Dog("Sharik");
        Pet pet2 = new DomesticCat("Murzik");
        family1.addPet(pet1);
        family1.addPet(pet2);
        //when
        Set<Pet> result = familyService.getPets(0);
        //then
        Set<Pet> expected = new HashSet<>(Arrays.asList(pet1, pet2));
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void isAddPet_addsPetToSet() {
        //given
        Pet pet1 = new Dog("Sharik");
        //when
        familyService.addPet(0, pet1);
        Set<Pet> result = newCollection.getFamilyByIndex(0).getPets();
        //then
        Set<Pet> expected = new HashSet<>(Arrays.asList(pet1));
        assertThat(result).isEqualTo(expected);
    }
}