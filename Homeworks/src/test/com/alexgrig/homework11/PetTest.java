package com.alexgrig.homework11;


import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PetTest {
    @Test
    public void IsToStringReturnsCorrectString() {

        Pet newPet = new Dog("Bobik", 3, 52, "eat", "sleep");
        String result = newPet.toString();
        assertThat(result).isEqualTo("DOG, cannot fly, has fur, number of legs: 4, {nickname='Bobik', age=3, trickLevel=52, habits=[sleep, eat]}");
    }
}
