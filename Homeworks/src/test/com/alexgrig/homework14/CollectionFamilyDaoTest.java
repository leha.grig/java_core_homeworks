package com.alexgrig.homework14;

import com.alexgrig.homework14.humans.Human;
import com.alexgrig.homework14.humans.Man;
import com.alexgrig.homework14.humans.Woman;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CollectionFamilyDaoTest {
    private static Human mam1;
    private static Human dad1;
    private static Human mam2;
    private static Human dad2;
    private static Family family1;
    private static Family family2;
    private static CollectionFamilyDao newCollection;


    @Before
    public void executedBeforeEach() {
        mam1 = new Woman("Ann", "Smith", 1952);
        mam2 = new Woman("Mary", "MacCoin", 1975);
        dad1 = new Man("Bill", "Smith", 1950);
        dad2 = new Man("Charles", "MacBit", 1972);
        family1 = new Family(mam1, dad1);
        family2 = new Family(mam2, dad2);
        newCollection = new CollectionFamilyDao(family1, family2);
    }

    @Test
    public void IsGetFamilyByIndexReturnsNullWhenIndexBelowZero() {
        //given

        //when
        Family result = newCollection.getFamilyByIndex(-1);
        //then
        assertThat(result).isNull();
    }

    @Test
    public void IsGetFamilyByIndexReturnsNullWhenIndexLargerThenFamiliesSize() {
        //given

        //when
        Family result = newCollection.getFamilyByIndex(2);
        //then
        assertThat(result).isNull();
    }

    @Test
    public void IsGetFamilyByIndexReturnsCorrectFamily() {
        //given

        //when
        Family result = newCollection.getFamilyByIndex(1);
        //then
        assertThat(result).isEqualTo(family2);
    }

    @Test
    public void IsDeleteFamilyByIndexReturnsFalseWhenIndexBelowZero() {
        //given

        //when
        boolean result = newCollection.deleteFamily(-1);
        //then
        assertThat(result).isFalse();
    }

    @Test
    public void IsDeleteFamilyByIndexReturnsFalseWhenIndexOutOfRange() {
        //given

        //when
        boolean result = newCollection.deleteFamily(2);
        //then
        assertThat(result).isFalse();
    }

    @Test
    public void IsDeleteFamilyReturnsTrueWhenFamilyDeleted() {
        //given

        //when
        boolean result = newCollection.deleteFamily(1);
        //then
        assertThat(result).isTrue();
    }

    @Test
    public void IsDeleteFamilyByIndexDeletesTheCorrectFamily() {
        //given

        //when
        newCollection.deleteFamily(1);
        //then
        assertThat(newCollection.getAllFamilies()).contains(family1).doesNotContain(family2);
    }

    @Test
    public void isDeleteFamilyByObjectReturnsFalseWhenNoSuchFamily() {
        //given
        CollectionFamilyDao singleCollection = new CollectionFamilyDao(family1);
        //when
        boolean result = singleCollection.deleteFamily(family2);
        //then
        assertThat(result).isFalse();
    }

    @Test
    public void isDeleteFamilyByObjectReturnsTrueWhenFamilyDeleted() {
        //given

        //when
        boolean result = newCollection.deleteFamily(family2);
        //then
        assertThat(result).isTrue();
    }

    @Test
    public void IsDeleteFamilyByObjectDeletesTheCorrectFamily() {
        //given

        //when
        newCollection.deleteFamily(family2);
        //then
        assertThat(newCollection.getAllFamilies()).contains(family1).doesNotContain(family2);
    }


    @Test
    public void isSaveFamilyReplaceTheObjectWhenItExistsInList() {
        //given
        Human child = new Man("Max", "Smith", 2002);
        family1.addChild(child);
        //when
        newCollection.saveFamily(family1);
        //then
        assertThat(newCollection.getFamilyByIndex(0).getChilds().get(0)).isEqualTo(child);
    }
    @Test
    public void isSaveFamilyAddTheObjectToListWhenItNotPresentInList() {
        //given
        Human mam3 = new Woman("Kate", "Fry", 1991);
        Human dad3 = new Man("Max", "Fry", 1990);
        Family family3 = new Family(mam3, dad3);
        //when
        newCollection.saveFamily(family3);
        //then
        assertThat(newCollection.getAllFamilies()).contains(family1, family2, family3);
    }

    @Test
    public void isSaveFamilyAddTheNewFamilyToTheEndOfListWhenItNotPresentInList() {
        //given
        Human mam3 = new Woman("Kate", "Fry", 1991);
        Human dad3 = new Man("Max", "Fry", 1990);
        Family family3 = new Family(mam3, dad3);
        //when
        newCollection.saveFamily(family3);
        //then
        assertThat(newCollection.getFamilyByIndex(2)).isEqualTo(family3);
    }
}