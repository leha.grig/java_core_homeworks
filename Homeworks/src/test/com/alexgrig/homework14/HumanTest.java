package com.alexgrig.homework14;

import com.alexgrig.homework14.humans.Human;
import com.alexgrig.homework14.humans.Woman;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class HumanTest {

    @Test
    public void IsToStringReturnsCorrectString() {

        Human newWoman = new Woman("Olena", "Smith", 1980);
        String result = newWoman.toString();
        assertThat(result).isEqualTo("{Olena Smith, birthDate=01/01/1980, IQLevel=0, schedule=null}");
    }
}
