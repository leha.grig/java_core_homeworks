package com.alexgrig.homework9;

import org.junit.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FamilyTest {
    private static Human mother;
    private static Human father;
    private static Human child1;
    private static Human child2;


    @Before
    public void executedBeforeEach() {
        mother = new Woman("Mum", "Smith", 1980);
        father = new Man("Dad", "Smith", 1978);
        child1 = new Woman("Masha", "Smith", 2002);
        child2 = new Man("Petya", "Smith", 2005);
    }

    @Test
    public void IsToStringReturnsCorrectString() {

        // вариант с mock, если без создания реальных объектов mother и father. Но классы для mock не должны быть финальными!:
        /*Human mother = mock(Woman.class);
        when(mother.toString()).thenReturn("mum");
        Human father = mock(Man.class);
        when(father.toString()).thenReturn("dad");*/

        Family newFamily = new Family(mother, father);
        String result = newFamily.toString();
        assertThat(result).isEqualTo("Family{mother=Human{name='Mum', surname='Smith', yearOfBirth=1980, IQLevel=0, schedule=null}, father=Human{name='Dad', surname='Smith', yearOfBirth=1978, IQLevel=0, schedule=null}, childs=[], pet=null}");
    }

    @Test
    public void checkTheChildsArrayIsLongerAfterAddingNewChild() {
        //given
        Family newFamily = new Family(mother, father);
        //when
        int lengthInitial = newFamily.getChilds().length;
        newFamily.addChild(child1);

        //then
        Human[] result = newFamily.getChilds();
        assertThat(result.length).isEqualTo(lengthInitial + 1);
    }

    @Test
    public void checkTheChildIsAddedToChildsArray() {
        //given
        Family newFamily = new Family(mother, father);
        //when
        newFamily.addChild(child1);

        //then
        Human[] result = newFamily.getChilds();
        assertThat(result).contains(child1);
    }

    @Test
    public void checkTheWrongChildDeletionDoesNotChangeChildsArray() {
        //given
        Family newFamily = new Family(mother, father);
        newFamily.addChild(child1);
        //when
        Human[] childs = newFamily.getChilds();
        newFamily.deleteChild(child2);
        //then
        Human[] result = newFamily.getChilds();
        assertThat(childs).isEqualTo(result);
    }

    @Test
    public void checkTheCorrectChildDeletedFromChildArray() {
        //given
        Family newFamily = new Family(mother, father);
        newFamily.addChild(child1);
        newFamily.addChild(child2);
        //when
        newFamily.deleteChild(child1);
        //then
        assertThat(newFamily.getChilds()).contains(child2).doesNotContain(child1);
    }

    @Test
    public void checkTheDeleteChildReturnsTrueAfterDeletionByIndex() {
        //given
        Family newFamily = new Family(mother, father);
        newFamily.addChild(child1);
        newFamily.addChild(child2);
        //when
        boolean result = newFamily.deleteChild(1);
        //then
        assertThat(result).isEqualTo(true);
    }

    @Test
    public void checkTheChildReallyDeletedFromChildArrayByIndex() {
        //given
        Family newFamily = new Family(mother, father);
        newFamily.addChild(child1);
        newFamily.addChild(child2);
        //when
        newFamily.deleteChild(1);
        //then
        assertThat(newFamily.getChilds()).contains(child1).doesNotContain(child2);
    }

    @Test
    public void checkTheWrongIndexDeletionDoesNotChangeChildsArray() {
        //given
        Family newFamily = new Family(mother, father);
        newFamily.addChild(child1);
        //when
        Human[] childs = newFamily.getChilds();
        newFamily.deleteChild(1);
        //then
        Human[] result = newFamily.getChilds();
        assertThat(childs).isEqualTo(result);
    }

    @Test
    public void checkTheDeleteChildReturnsFalseIfNotDeletedByIndex() {
        //given
        Family newFamily = new Family(mother, father);
        newFamily.addChild(child1);
        //when
        boolean result = newFamily.deleteChild(1);
        //then
        assertThat(result).isEqualTo(false);
    }

    @Test
    public void shouldReturnCorrectNumberOfFamilyMembers() {
        //given
        Family newFamily = new Family(mother, father);
        newFamily.addChild(child1);
        newFamily.addChild(child2);
        //when

        //then
        assertThat(newFamily.countFamily()).isEqualTo(4);
    }


    @Test
    public void shouldEqualsReturnFalseWhenDifferentParents() {
        //given
        Family family1 = new Family(mother, father);
        Family family2 = new Family(child1, child2);
        family1.addChild(child1);
        family2.addChild(child2);
        //when

        //then
        assertThat(family1.equals(family2)).isFalse();
    }

    @Test
    public void shouldEqualsReturnTrueWhenSameParents() {
        //given
        Family family1 = new Family(mother, father);
        Family family2 = new Family(mother, father);
        family1.addChild(child1);
        family2.addChild(child2);
        //when

        //then
        assertThat(family1.equals(family2)).isTrue();
    }

    // рефлексивность, транзитивность, симметричность, консистентность
    @Test
    public void EqualsReflection() {
        //given
        Family family1 = new Family(mother, father);
        //when
        //then
        assertThat(family1.equals(family1)).isTrue();
    }

    @Test
    public void EqualsSymmetry() {
        //given
        Family family1 = new Family(mother, father);
        Family family2 = new Family(mother, father);
        family1.addChild(child1);
        family2.addChild(child2);
        //when
        boolean result1 = family1.equals(family2);
        boolean result2 = family2.equals(family1);
        //then
        assertThat(result1).isEqualTo(result2);
    }

    @Test
    public void EqualsTransition() {
        //given
        Family family1 = new Family(mother, father);
        Family family2 = new Family(mother, father);
        Family family3 = new Family(mother, father);
        family1.addChild(child1);
        family2.addChild(child2);
        //when
        boolean result1 = family1.equals(family2);
        boolean result2 = family2.equals(family3);
        //then
        if (result1 && result2) {
            assertThat(family1.equals(family3)).isTrue();
        }
    }

    @Test
    public void EqualsConsistency() {
        //given
        Family family1 = new Family(mother, father);
        Family family2 = new Family(mother, father);
        family1.addChild(child1);
        family2.addChild(child2);
        //when
        for (int i = 0; i == 10; i++) {
            boolean temp = family1.equals(family2);
        }
        //then
        assertThat(family1.equals(family2)).isTrue();
    }

    @Test
    public void EqualObjectsMustHaveSameHashcode() {
        //given
        Family family1 = new Family(mother, father);
        Family family2 = new Family(mother, father);
        Family family3 = new Family(child1, child2);
        family1.addChild(child1);
        family2.addChild(child2);
        //when
        boolean result1 = family1.equals(family2);
        //then
        if (result1) {
            assertThat(family1.hashCode()).isEqualTo(family2.hashCode())
                    .isNotEqualTo(family3.hashCode());
        }
    }
}
