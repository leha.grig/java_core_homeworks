package com.alexgrig.homework13;

import com.alexgrig.homework13.humans.Human;
import com.alexgrig.homework13.humans.HumanSchedule;
import com.alexgrig.homework13.humans.Man;
import com.alexgrig.homework13.humans.Woman;
import com.alexgrig.homework13.pets.DireWolf;
import com.alexgrig.homework13.pets.Dog;
import com.alexgrig.homework13.pets.DomesticCat;
import com.alexgrig.homework13.pets.Pet;

public class Main {
    public static void main(String[] args) {


        Pet summerWolf = new DireWolf("Summer");
        summerWolf.setAge(2);
        summerWolf.setTrickLevel(57);
        summerWolf.setHabits("eat meat", "hunt", "growl");

        Human eddardStark = new Man("Eddard", "Stark", 1000);
        HumanSchedule eddardSchedule = new HumanSchedule("praying to Tree", "catch some wildlings or deserters", "judge", "execute the judgment", "manage Winterfell", "family communication, parenting", "family communication, parenting");
        eddardStark.setSchedule(eddardSchedule.getSchedule());
        eddardStark.setIQLevel(50);


        Human catelynStark = new Woman("Catelyn", "Stark", 1010);
        HumanSchedule catelynSchedule = new HumanSchedule("praying to Tree", "manage Winterfell", "husband care", "manage Winterfell", "parenting", "parenting", "parenting");
        catelynStark.setSchedule(catelynSchedule.getSchedule());
        catelynStark.setIQLevel(60);


        HumanSchedule brandonSchedule = new HumanSchedule("climb on roofs", "climb on roofs", "climb on roofs", "climb on roofs", "climb on roofs", "climb on roofs", "fall down");
        Human brandonStark = new Human("Brandon", "Stark", 1037, 70, brandonSchedule.getSchedule());


        Family starks = new Family(catelynStark, eddardStark);
        starks.addPet(summerWolf);
        starks.addChild(brandonStark);

        Pet matroskin = new DomesticCat("Matroskin", 5, 75, "sew", "care about cow");
        Pet sharik = new Dog();

        Human papaFedora = new Man("Papa", "F", 1945);
        Human mamaFedora = new Woman("Mama", "F", 1950);
        Human dyadyaFedor = new Man ("Fedor", "Dydya", 1970);
        Human masha = new Woman("Maria", "Prosto", 1980);
        Human pedro = new Man ("Pedro", "Don", 1980);

        Family fedors = new Family (mamaFedora, papaFedora);
        fedors.addPet(matroskin);
//        fedors.addChild(dyadyaFedor);

        FamilyDao newFamilyDao = new CollectionFamilyDao(starks, fedors);
        FamilyService familyService = new FamilyService(newFamilyDao);
        FamilyController familyController = new FamilyController(familyService);

// вызов методов по заданию, проверка корректности:

        System.out.println("");
        familyController.displayAllFamilies();
        System.out.println("");
        familyController.getFamiliesBiggerThan(2);
        System.out.println("");
        familyController.getFamiliesLessThan(3);
        System.out.println("");
        System.out.println(familyController.countFamiliesWithMemberNumber(2));
        System.out.println(familyController.count());
        familyController.createNewFamily(masha, pedro);
        System.out.println(familyController.getAllFamilies());
        familyController.deleteFamilyByIndex(2);
        familyController.bornChild(fedors, "Kruzershtern", "Murka");
        System.out.println(fedors);
        familyController.adoptChild(fedors, masha);
        familyController.deleteAllChildrenOlderThen (70);
        familyController.getFamilyById(0);
        familyController.getPets(1);
        familyController.addPet(1, sharik);

//        System.out.println("qW1".matches("[^A-Za-z]+"));





        /*System.out.println(brandonStark);
        System.out.println(summerWolf);
        starks.bornChild();
        System.out.println(starks);
        System.out.println(Arrays.toString(starks.getChilds()));
        System.out.println(Arrays.toString(brandonStark.getFamily().getChilds()));*/
    }
}
