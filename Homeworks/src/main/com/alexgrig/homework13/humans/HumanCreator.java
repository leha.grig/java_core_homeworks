package com.alexgrig.homework13.humans;

public interface HumanCreator {
    Human bornChild();
}
