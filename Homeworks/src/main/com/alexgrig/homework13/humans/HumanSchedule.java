package com.alexgrig.homework13.humans;

import java.util.HashMap;
import java.util.Map;

public class HumanSchedule {
    private final Map<String, String> schedule;

    public HumanSchedule(String suTask, String moTask, String tuTask, String weTask, String thTask, String frTask, String saTask){
        schedule = new HashMap<>();
        schedule.put(DayOfWeek.SUNDAY.name(), suTask);
        schedule.put(DayOfWeek.MONDAY.name(), moTask);
        schedule.put(DayOfWeek.TUESDAY.name(), tuTask);
        schedule.put(DayOfWeek.WEDNESDAY.name(), weTask);
        schedule.put(DayOfWeek.THURSDAY.name(), thTask);
        schedule.put(DayOfWeek.FRIDAY.name(), frTask);
        schedule.put(DayOfWeek.SATURDAY.name(), saTask);

    }
    /*
    //Вариант через "двумерный" ArrayList:
    private final List <ArrayList<String>> schedule;

    public HumanSchedule(String suTask, String moTask, String tuTask, String weTask, String thTask, String frTask, String saTask){
        schedule = new ArrayList <>();
        for(int i=0; i<7; i++){
            schedule.add(new ArrayList<>());
            schedule.get(i).add(DayOfWeek.values()[i].name());
        }
        schedule.get(0).add(1, suTask);
        schedule.get(1).add(1, moTask);
        schedule.get(2).add(1, tuTask);
        schedule.get(3).add(1, weTask);
        schedule.get(4).add(1, thTask);
        schedule.get(5).add(1, frTask);
        schedule.get(6).add(1, saTask);
    }*/

    public Map<String, String> getSchedule() {
        return schedule;
    }

    @Override
    public String toString() {
        return "HumanSchedule{" +
                "schedule=" + schedule.toString() +
                '}';
    }
}
