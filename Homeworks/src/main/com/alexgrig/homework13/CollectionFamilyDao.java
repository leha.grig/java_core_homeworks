package com.alexgrig.homework13;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> families;

    CollectionFamilyDao() {
    }

    CollectionFamilyDao(List<Family> families) {
        this.families = families;
    }

    CollectionFamilyDao(Family... families) {
        this.families = new ArrayList<>(Arrays.asList(families));
    }


    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (families.size()<=index || index < 0){
            return null;
        }
        return families.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        if (families.size() > index && index >= 0) {
            families.remove(index);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (family == null) {
            return false;
        }
        boolean result = families.remove(family);
        return result;
    }

    @Override
    public void saveFamily(Family family) {
        int ind = families.indexOf(family);
        if (ind >= 0) {
            families.set(ind, family);
        } else {
            families.add(family);
        }
    }
}
