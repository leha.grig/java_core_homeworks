package com.alexgrig.homework2;

import java.util.Random;
import java.util.Scanner;

// вариант: сделать проверку на число регулярными выражениями, считывая сканером строку


public class ShootingGame {
    private int[][] field;
    private final int FIELD_LENGTH;
    private final int FIELD_HEIGHT;
    private final Scanner SCAN;
    private final Random RANDOM = new Random();
    private int counter = 0;


    public ShootingGame(int a, int b, Scanner scan) {
        this.FIELD_LENGTH = a + 1;
        this.FIELD_HEIGHT = b + 1;
        this.SCAN = scan;
    }

    public void setTheField() {

        field = new int[FIELD_LENGTH][FIELD_HEIGHT];

        for (int i = 0; i < FIELD_LENGTH; i++) {
            field[0][i] = i;
        }
        for (int i = 0; i < FIELD_HEIGHT; i++) {
            field[i][0] = i;
        }

        // set a target 3x1 or 1x3
        boolean direction = RANDOM.nextBoolean();
        if (direction) {
            int x = (int) (1 + Math.random() * (FIELD_LENGTH - 3));
            int y = (int) (1 + Math.random() * (FIELD_HEIGHT - 1));
            field[x][y] = -5;
            field[x + 1][y] = -5;
            field[x + 2][y] = -5;
        } else {
            int x = (int) (1 + Math.random() * (FIELD_LENGTH - 1));
            int y = (int) (1 + Math.random() * (FIELD_HEIGHT - 3));
            field[x][y] = -5;
            field[x][y + 1] = -5;
            field[x][y + 2] = -5;
        }

    }

    public void drawTheField() {
        // коды:
        // нетронутое поле: 0 (по умолчанию); поле с целью: -5; поле с выстрелом мимо: -1; поле с выстрелом по цели: -2
        for (int i = 0; i < FIELD_LENGTH; i++) {
            for (int j = 0; j < FIELD_HEIGHT; j++) {
                if (i == 0 && j == 0) {
                    System.out.print(" 0 |");
                } else if (field[i][j] > 0) {
                    System.out.print(" " + field[i][j] + " |");
                } else if (field[i][j] == -1) {
                    System.out.print(" * |");
                } else if (field[i][j] == -2) {
                    System.out.print(" x |");
                } else {
                    System.out.print(" - |");
                }
            }
            System.out.println();
        }
    }

    public void shooting() {
        System.out.println("All set. Get ready to rumble!");

        while (true) {

            System.out.println("Enter the string number (form 1 to " + (FIELD_HEIGHT - 1) + "): ");
            checkTheScannedNumber();
            int x = SCAN.nextInt();
            x = checkTheNumberRange(x, 1, FIELD_HEIGHT);

            System.out.println("Enter the row number (form 1 to " + (FIELD_LENGTH - 1) + "): ");
            checkTheScannedNumber();
            int y = SCAN.nextInt();
            y = checkTheNumberRange(y, 1, FIELD_LENGTH);

            if (field[x][y] == -5) {
                field[x][y] = -2;
                counter++;
                drawTheField();
                if (counter == 3) {
                    System.out.println("You have won!");
                    counter = 0;
                    break;
                }
            } else {
                field[x][y] = -1;
                drawTheField();
            }
        }
    }

    public void checkTheScannedNumber() {
        while (!SCAN.hasNextInt()) { // Проверка на число
            System.out.println("That not a number! Please, try again.");
            SCAN.next();
        }
    }

    public int checkTheNumberRange(int number, int rangeStart, int rangeFinal) {
        while (number >= rangeFinal || number < rangeStart) {
            System.out.println("The number is out of range! Please, try again.");
            number = SCAN.nextInt();
        }
        return number;
    }
}
