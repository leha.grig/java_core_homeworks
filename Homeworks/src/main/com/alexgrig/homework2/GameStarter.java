package com.alexgrig.homework2;

import java.util.Scanner;

public class GameStarter {
    public static void main(String[] args) {
        ShootingGame newGame = new ShootingGame(5,5, new Scanner(System.in));
        newGame.setTheField();
        newGame.drawTheField();
        newGame.shooting();
    }
}
