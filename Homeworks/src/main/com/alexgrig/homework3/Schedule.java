package com.alexgrig.homework3;

import java.util.Scanner;

public class Schedule {
    private final String[][] SCHEDULE;
    private final Scanner SCAN = new Scanner(System.in);

    public Schedule() {
        SCHEDULE = new String[7][2];
        SCHEDULE[0][0] = "Sunday";
        SCHEDULE[0][1] = "Do Sunday rest";
        SCHEDULE[1][0] = "Monday";
        SCHEDULE[1][1] = "Do Monday job";
        SCHEDULE[2][0] = "Tuesday";
        SCHEDULE[2][1] = "Do Tuesday job";
        SCHEDULE[3][0] = "Wednesday";
        SCHEDULE[3][1] = "Do Wednesday job";
        SCHEDULE[4][0] = "Thursday";
        SCHEDULE[4][1] = "Do Thursday job";
        SCHEDULE[5][0] = "Friday";
        SCHEDULE[5][1] = "Do Friday job";
        SCHEDULE[6][0] = "Saturday";
        SCHEDULE[6][1] = "Do Saturday rest";
    }

    public void output() {
        mainCicle: // не все поощряют использование label. Вариант лучшего кода без них см. ниже. Так же для выхода из цикла можно
        // было создать буль-флаг и менять его при введении пользователем exit
        while (true) {
            System.out.println("Please, input the day of the week:");
            String dayEntered = SCAN.nextLine();
            dayEntered = dayEntered.trim().toLowerCase();
            if (dayEntered.indexOf("change") == 0 || dayEntered.indexOf("reschedule") == 0) { // условие входа в цикл - только в случае намериния пользователя поменять таску
                for (int i = 0; i < SCHEDULE.length; i++) {
                    String tempStr1 = "change " + SCHEDULE[i][0];
                    String tempStr2 = "reschedule " + SCHEDULE[i][0];
                    if (dayEntered.equalsIgnoreCase(tempStr1) || dayEntered.equalsIgnoreCase(tempStr2)) {
                        System.out.println("Please, input new tasks for " + SCHEDULE[i][0]);
                        String newTask = SCAN.nextLine();
                        SCHEDULE[i][1] = newTask;

                        continue mainCicle;
                    }
                }
            }
            switch (dayEntered) {
                case "sunday":
                    System.out.println("Your tasks for Sunday: " + SCHEDULE[0][1]);
                    break;
                case "monday":
                    System.out.println("Your tasks for Monday: " + SCHEDULE[1][1]);
                    break;
                case "tuesday":
                    System.out.println("Your tasks for Tuesday: " + SCHEDULE[2][1]);
                    break;
                case "wednesday":
                    System.out.println("Your tasks for Wednesday: " + SCHEDULE[3][1]);
                    break;
                case "thursday":
                    System.out.println("Your tasks for Thursday: " + SCHEDULE[4][1]);
                    break;
                case "friday":
                    System.out.println("Your tasks for Friday: " + SCHEDULE[5][1]);
                    break;
                case "saturday":
                    System.out.println("Your tasks for Saturday: " + SCHEDULE[6][1]);
                    break;
                case "exit":
                    break mainCicle;
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");

            }
        }
    }
}

// refactoring by Michael Kutsenko:
/*

import java.util.Scanner;

public class Schedule {
    private final String[][] SCHEDULE;
    private final Scanner SCAN = new Scanner(System.in);

    public static void main(String[] args) {
        Schedule newSchedule = new Schedule();
        newSchedule.output();
    }


    public Schedule() {
        SCHEDULE = new String[7][2];
        SCHEDULE[0][0] = "Sunday";
        SCHEDULE[0][1] = "Do Sunday rest";
        SCHEDULE[1][0] = "Monday";
        SCHEDULE[1][1] = "Do Monday job";
        SCHEDULE[2][0] = "Tuesday";
        SCHEDULE[2][1] = "Do Tuesday job";
        SCHEDULE[3][0] = "Wednesday";
        SCHEDULE[3][1] = "Do Wednesday job";
        SCHEDULE[4][0] = "Thursday";
        SCHEDULE[4][1] = "Do Thursday job";
        SCHEDULE[5][0] = "Friday";
        SCHEDULE[5][1] = "Do Friday job";
        SCHEDULE[6][0] = "Saturday";
        SCHEDULE[6][1] = "Do Saturday rest";

    }

    public void output() {


        while (true) {
            System.out.println("Please, input the day of the week:");
            String dayEntered = SCAN.nextLine();
            dayEntered = dayEntered.trim().toLowerCase();

            if (dayEntered.equals("exit")) {
                break;

            } else if (isTaskReschedule(dayEntered)) { // условие входа в цикл - только в случае намериния пользователя поменять таску
                reschedule(dayEntered);

            } else {
                showTask(dayEntered);

            }
        }
    }

    private boolean isTaskReschedule(String input) {
        return input.indexOf("change") == 0 || input.indexOf("reschedule") == 0;
    }


    private void reschedule(String dayEntered) {
        for (int i = 0; i < SCHEDULE.length; i++) {
            String tempStr1 = "change " + SCHEDULE[i][0];
            String tempStr2 = "reschedule" + SCHEDULE[i][0];

            if (dayEntered.equalsIgnoreCase(tempStr1) || dayEntered.equalsIgnoreCase(tempStr2)) {
                System.out.println("Please, input new tasks for " + SCHEDULE[i][0]);
                String newTask = SCAN.nextLine();

                if (newTask.equalsIgnoreCase("exit")) {
                    return;
                }

                SCHEDULE[i][1] = newTask;
            }
        }
    }

    private void showTask(String dayEntered) {
        switch (dayEntered) {
            case "sunday":
                System.out.println("Your tasks for Sunday: " + SCHEDULE[0][1]);
                break;
            case "monday":
                System.out.println("Your tasks for Monday: " + SCHEDULE[1][1]);
                break;
            case "tuesday":
                System.out.println("Your tasks for Tuesday: " + SCHEDULE[2][1]);
                break;
            case "wednesday":
                System.out.println("Your tasks for Wednesday: " + SCHEDULE[3][1]);
                break;
            case "thursday":
                System.out.println("Your tasks for Thursday: " + SCHEDULE[4][1]);
                break;
            case "friday":
                System.out.println("Your tasks for Friday: " + SCHEDULE[5][1]);
                break;
            case "saturday":
                System.out.println("Your tasks for Saturday: " + SCHEDULE[6][1]);
                break;
            default:
                System.out.println("Sorry, I don't understand you, please try again.");
        }
    }
}*/
