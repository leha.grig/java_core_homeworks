package com.alexgrig.homework8;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human[] childs;
    private Pet pet;

    static {
        System.out.println("The new class Family is loading");
    }

    {
        System.out.println("The new object Family is creating");
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.childs = new Human[0];
        this.mother.setFamily(this);
        this.father.setFamily(this);
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public Human[] getChilds() {
        return childs;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void addChild(Human child) {
        childs = Arrays.copyOf(childs, childs.length + 1);
        childs[childs.length - 1] = child;
        child.setFamily(this);
    }

    public boolean deleteChild(int i) {
        if (i >= childs.length || i<0) {
            return false;
        }
        if (childs[i] != null) {
            childs[i].setFamily(null);
            Human[] temp = Arrays.copyOf(childs, childs.length - 1);
            System.arraycopy(childs, 0, temp, 0, i);
            System.arraycopy(childs, i + 1, temp, i, childs.length - i - 1);
            childs = temp;
            return true;
        } else {
            return false;
        }
    }

    public boolean deleteChild(Human child) {
        for (int i = 0; i < childs.length; i++) {
            Human elem = childs[i];
            if (elem.hashCode() == child.hashCode()) {
                if (elem.equals(child)) {
                    child.setFamily(null);
                    Human[] temp = Arrays.copyOf(childs, childs.length - 1);
                    System.arraycopy(childs, 0, temp, 0, i);
                    System.arraycopy(childs, i + 1, temp, i, childs.length - i - 1);
                    childs = temp;
                    return true;
                }
            }
        }
        return false;
    }


    public int countFamily() {
        return 2 + childs.length;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", childs=" + Arrays.toString(childs) +
                ", pet=" + pet +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("object deleted: " + this.toString());
    }
}
