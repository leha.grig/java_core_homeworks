package com.alexgrig.homework8;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Human {
    private static final Random RANDOM = new Random();
    private String name;
    private String surname;
    private int yearOfBirth;
    private int IQLevel;
    private String[][] schedule;
    private Family family;

    static {
        System.out.println("The new class Human is loading");
    }

    {
        System.out.println("The new object Human is creating");
    }

    public Human(String name, String surname, int year_of_birth) {
        this.name = name;
        this.surname = surname;
        yearOfBirth = year_of_birth;
    }

    public Human(String name, String surname, int yearOfBirth, int IQLevel, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
        this.IQLevel = IQLevel;
        this.schedule = schedule;
    }

    public Human(String name, String surname, int yearOfBirth, int IQLevel, String[][] schedule, Family family) {
        this.name = name;
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
        this.IQLevel = IQLevel;
        this.schedule = schedule;
        this.family = family;
    }

    public Human() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public int getIQLevel() {
        return IQLevel;
    }

    public void setIQLevel(int IQLevel) {
        this.IQLevel = IQLevel;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Human getMother() {
        return this.getFamily().getMother();
    }

    public Human getFather() {
        return this.getFamily().getFather();
    }

    public void greetPet() {
        System.out.println("Привет, " + family.getPet().getNickname());
    }

    public void describePet() {
        String tricky = family.getPet().getTrickLevel() <= 50 ? "почти не хитрый" : "очень хитрый";
        String yearsName = family.getPet().getAge() <= 4 ? "года" : "лет";
        if (family.getPet().getAge() == 1) {
            yearsName = "год";
        }
        System.out.println("У меня есть " + family.getPet().getSpecies() + ", ему " + family.getPet().getAge() + " " + yearsName + ", он " + tricky);
    }

    public Boolean feedPet(Boolean bool) {
        String feedingYes = "Хм... покормлю-ка я " + family.getPet().getNickname();
        String feedingNo = "Думаю, " + family.getPet().getNickname() + " не голоден.";
        if (bool) {
            System.out.println(feedingYes);
            return true;
        } else {
            int temp = RANDOM.nextInt(101);
            if (temp < family.getPet().getTrickLevel()) {
                System.out.println(feedingYes);
                return true;
            }
            System.out.println(feedingNo);
            return false;
        }
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", yearOfBirth=" + yearOfBirth +
                ", IQLevel=" + IQLevel +
                ", schedule=" + Arrays.deepToString(schedule) +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Human human = (Human) obj;
        return yearOfBirth == human.yearOfBirth &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, yearOfBirth) * 7;
    }

    @Override
    protected void finalize () throws Throwable {
        System.out.println(this.toString());
    }
}
