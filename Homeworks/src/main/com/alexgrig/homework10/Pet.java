package com.alexgrig.homework10;

import java.util.Arrays;
import java.util.Objects;

public abstract class Pet {
    private Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    static {
        System.out.println("The new class Pet is loading");
    }

    {
        System.out.println("The new object Pet is creating");
    }

    public Pet(String nickname) {
        this.nickname = nickname;
    }

    public Pet(String nickname, int age, int trickLevel, String... habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet() {
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void eat() {
        System.out.println("Я кушаю");
    }

    public abstract void respond();

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int level) {
        this.trickLevel = level;
    }

    public void setHabits(String... habits) {
        this.habits = habits;
    }

    public Species getSpecies() {
        return this.species;
    }

    public String getNickname() {
        return this.nickname;
    }

    public int getAge() {
        return this.age;
    }

    public int getTrickLevel() {
        return this.trickLevel;
    }

    public String[] getHabits() {
        return this.habits;
    }

    @Override
    public String toString() {
        String habitsExist = "no habits";
        if (this.getHabits() != null) {
            habitsExist = Arrays.toString(this.getHabits());
        }
        String canFly = "cannot fly";
        String hasFur = "has no fur";
        if(getSpecies().isCanFly()){canFly = "can fly";}
        if(getSpecies().isHasFur()){hasFur = "has fur";}

        return this.getSpecies() + ", " + canFly + ", " + hasFur + ", number of legs: "+ getSpecies().getNumberOfLegs() + ", {nickname='" + this.getNickname() + "', age=" + this.getAge() + ", trickLevel=" + this.getTrickLevel() + ", habits=" + habitsExist + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return trickLevel == pet.trickLevel &&
                Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, trickLevel) * 11;
    }

    @Override
    protected void finalize () throws Throwable {
        System.out.println(this.toString());
    }
}
