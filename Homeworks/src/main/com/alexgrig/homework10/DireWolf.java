package com.alexgrig.homework10;

public class DireWolf extends Pet implements PetFouling {
    public DireWolf (){
        super();
    }
    public DireWolf (String nickname){
        super(nickname);

    }
    public DireWolf (String nickname, int age, int trickLevel, String... habits) {
        super(nickname, age, trickLevel, habits);

    }
    {
        setSpecies(Species.DIREWOLF);
    }
    public void foul()  {
        System.out.println("Надо закопать остатки соседского барана...");
    }
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + getNickname() + " Идем охотиться!");
    }
}
