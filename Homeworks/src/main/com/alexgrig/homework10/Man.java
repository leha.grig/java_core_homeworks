package com.alexgrig.homework10;

public final class Man extends Human {
    public Man (){
        super();
    }
    public Man (String name, String surname, int yearOfBirth){
        super(name, surname, yearOfBirth);
    }
    public Man (String name, String surname, int yearOfBirth, int IQLevel, String[][] schedule){
        super(name, surname, yearOfBirth, IQLevel, schedule);
    }
    public Man (String name, String surname, int yearOfBirth, int IQLevel, String[][] schedule, Family family) {
        super(name, surname, yearOfBirth, IQLevel, schedule, family);
    }
    public void searchSocks () {
        System.out.println("Пойду опять в разных, все равно в ботинках");
    }

    @Override
    public void greetPet() {
        System.out.println("Привет, " + getFamily().getPet().getNickname() + ". Я тоже рад тебя видеть");
    }
}
