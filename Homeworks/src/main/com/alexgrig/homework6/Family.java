package com.alexgrig.homework6;

import java.util.Arrays;

public class Family {
    private Human mother;
    private Human father;
    private Human[] childs;
    private Pet pet;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.childs = new Human[0];
        this.mother.setFamily(this);
        this.father.setFamily(this);
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public Human[] getChilds() {
        return childs;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void addChild(Human child) {
        this.childs = Arrays.copyOf(this.childs, this.childs.length + 1);
        this.childs[this.childs.length - 1] = child;
        child.setFamily(this);
    }

    public boolean deleteChild(int i) {
        if (i>=childs.length || i<0){
            return false;
        }
        if (this.childs[i] != null) {
            this.childs[i].setFamily(null);
            Human[] temp = Arrays.copyOf(this.childs, this.childs.length - 1);
            System.arraycopy(this.childs, 0, temp, 0, i);
            System.arraycopy(this.childs, i + 1, temp, i, this.childs.length - i - 1);
            this.childs = temp;
            return true;
        } else {
            return false;
        }
    }

    public boolean deleteChild(Human child) {
        for (int i = 0; i < childs.length; i++) {
            Human elem = childs[i];
            if (elem.getName().equals(child.getName()) && elem.getSurname().equals(child.getSurname()) && elem.getYearOfBirth() == child.getYearOfBirth()) {
                child.setFamily(null);
                Human[] temp = Arrays.copyOf(this.childs, this.childs.length - 1);
                System.arraycopy(this.childs, 0, temp, 0, i);
                System.arraycopy(this.childs, i + 1, temp, i, this.childs.length - i - 1);
                this.childs = temp;
                return true;
            }
        }
        return false;
    }


    public int countFamily() {
        return 2 + childs.length;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", childs=" + Arrays.toString(childs) +
                ", pet=" + pet +
                '}';
    }
}
