package com.alexgrig.homework4;

public class HumanSchedule {
    private final String[][] SCHEDULE;

    public HumanSchedule(String suTask, String moTask, String tuTask, String weTask, String thTask, String frTask, String saTask){
        SCHEDULE = new String[7][2];
        SCHEDULE[0][0] = "Sunday";
        SCHEDULE[0][1] = suTask;
        SCHEDULE[1][0] = "Monday";
        SCHEDULE[1][1] = moTask;
        SCHEDULE[2][0] = "Tuesday";
        SCHEDULE[2][1] = tuTask;
        SCHEDULE[3][0] = "Wednesday";
        SCHEDULE[3][1] = weTask;
        SCHEDULE[4][0] = "Thursday";
        SCHEDULE[4][1] = thTask;
        SCHEDULE[5][0] = "Friday";
        SCHEDULE[5][1] = frTask;
        SCHEDULE[6][0] = "Saturday";
        SCHEDULE[6][1] = saTask;
    }

    public String[][] getSCHEDULE() {
        return SCHEDULE;
    }
}
