package com.alexgrig.homework4;

import java.util.Arrays;
import java.util.Random;

public class Human {
    private final Random RANDOM = new Random();
    private final String NAME;
    private final String SURNAME;
    private final int YEAR_OF_BIRTH;
    private Human mother;
    private Human father;
    private int IQLevel;
    private String[][] schedule;
    private Pet pet;

    public Human(String name, String surname, int year_of_birth) {
        NAME = name;
        SURNAME = surname;
        YEAR_OF_BIRTH = year_of_birth;
    }

    public String getNAME() {
        return NAME;
    }

    public String getSURNAME() {
        return SURNAME;
    }

    public int getYEAR_OF_BIRTH() {
        return YEAR_OF_BIRTH;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

     Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public int getIQLevel() {
        return IQLevel;
    }

    public void setIQLevel(int IQLevel) {
        this.IQLevel = IQLevel;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void greetPet() {
        System.out.println("Привет, " + pet.getNickname());
    }

    public void describePet() {
        String tricky = pet.getTrickLevel() <= 50 ? "почти не хитрый" : "очень хитрый";
        String yearsName = pet.getAge() <= 4 ? "года" : "лет";
        if (pet.getAge() == 1) {
            yearsName = "год";
        }
        System.out.println("У меня есть " + pet.getSpecies() + ", ему " + pet.getAge() + " " + yearsName + ", он " + tricky);
    }

    public Boolean feedPet(Boolean bool) {
        String feedingYes = "Хм... покормлю-ка я " + pet.getNickname();
        String feedingNo = "Думаю, " + pet.getNickname() + " не голоден.";
        if (bool) {
            System.out.println(feedingYes);
            return true;
        } else {
            int temp = RANDOM.nextInt(101);
            if (temp < pet.getTrickLevel()) {
                System.out.println(feedingYes);
                return true;
            }
            System.out.println(feedingNo);
            return false;
        }
    }

    @Override
    public String toString() {
        String petExist = "has no pet";
        if (this.getPet() != null) {
            String habitsExist = "no habits";
            if (this.getPet().getHabits() != null) {
                habitsExist = Arrays.toString(this.getPet().getHabits());
            }
            petExist = "pet=" + this.getPet().getSpecies() + "{nickname='" + this.getPet().getNickname() + "', age=" + this.getPet().getAge() + ", trickLevel=" + this.getPet().getTrickLevel() + ", habits=" + habitsExist + "}";
        }
        return "Human{name='" + this.getNAME() + "', surname='" + this.getSURNAME() + "', year=" + this.getYEAR_OF_BIRTH() + ", iq=" + this.getIQLevel() + ", mother=" + this.getMother().getNAME() + " " + this.getMother().getSURNAME() + ", father=" + this.getFather().getNAME() + " " + this.getFather().getSURNAME() + ", " + petExist + "}";
    }
}
