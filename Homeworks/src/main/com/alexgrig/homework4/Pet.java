package com.alexgrig.homework4;

import java.util.Arrays;

public class Pet {
    private final String SPEСIES;
    private final String NICKNAME;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Pet(String species, String nickname) {
        this.SPEСIES = species;
        this.NICKNAME = nickname;
    }

    public void eat(){
        System.out.println("Я кушаю");
    }
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + NICKNAME + " Я соскучился!");
    }
    public void foul (){
        System.out.println("Нужно хорошо замести следы...");
    }
    public void setAge (int age){
        this.age = age;
    }
    public void setTrickLevel (int level){
        this.trickLevel = level;
    }
    public void setHabits(String ... habits) {
        this.habits = habits;
    }
    public String getSpecies () {
        return this.SPEСIES;
    }
    public String getNickname () {
        return this.NICKNAME;
    }
    public int getAge () {
        return this.age;
    }
    public int getTrickLevel () {
        return this.trickLevel;
    }
    public String[] getHabits() {
        return this.habits;
    }

    @Override
    public String toString() {
        String habitsExist = "no habbits";
        if (this.getHabits() != null) {
            habitsExist = Arrays.toString(this.getHabits());
        }
        return this.getSpecies() + "{nickname='" + this.getNickname() + "', age=" + this.getAge() + ", trickLevel=" + this.getTrickLevel() + ", habits=" + habitsExist + "}";
    }
}
