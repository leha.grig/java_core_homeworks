package com.alexgrig.homework4;

public class Main {
    public static void main(String[] args) {
        Pet summerWolf = new Pet ("direwolf", "Summer");
        summerWolf.setAge(2);
        summerWolf.setTrickLevel(57);
        summerWolf.setHabits("eat meat", "hunt", "growl");

        Human eddardStark = new Human ("Eddard", "Stark", 1000);
        HumanSchedule eddardSchedule = new HumanSchedule("praying to Tree", "catch some wildlings or deserters", "judge", "execute the judgment", "manage Winterfell", "family communication, parenting", "family communication, parenting");
        eddardStark.setSchedule(eddardSchedule.getSCHEDULE());
        eddardStark.setIQLevel(50);

        Human catelynStark = new Human ("Catelyn", "Stark", 1010);
        HumanSchedule catelynSchedule = new HumanSchedule("praying to Tree", "manage Winterfell", "husband care", "manage Winterfell", "parenting", "parenting", "parenting");
        catelynStark.setSchedule(catelynSchedule.getSCHEDULE());
        catelynStark.setIQLevel(60);

        Human brandonStark = new Human ("Brandon", "Stark", 1037);
        HumanSchedule brandonSchedule = new HumanSchedule("climb on roofs", "climb on roofs", "climb on roofs", "climb on roofs", "climb on roofs", "climb on roofs", "fall down");
        brandonStark.setSchedule(brandonSchedule.getSCHEDULE());
        brandonStark.setIQLevel(70);
        brandonStark.setFather(eddardStark);
        brandonStark.setMother(catelynStark);
        brandonStark.setPet(summerWolf);

        // вызов методов по заданию:
        brandonStark.getPet().eat();
        brandonStark.getPet().foul();
        brandonStark.getPet().respond();
        brandonStark.greetPet();
        brandonStark.describePet();
        brandonStark.feedPet(false);
        System.out.println(brandonStark);
        System.out.println(brandonStark.getPet());
    }


}
