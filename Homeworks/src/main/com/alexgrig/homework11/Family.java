package com.alexgrig.homework11;

import java.util.*;

public class Family implements HumanCreator {
    private Human mother;
    private Human father;
    private List<Human> childs;
    private Set<Pet> pets = new HashSet<>();

    static {
        System.out.println("The new class Family is loading");
    }

    {
        System.out.println("The new object Family is creating");
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.childs = new ArrayList<>();
        this.mother.setFamily(this);
        this.father.setFamily(this);
    }

    public Human bornChild() {
        Human child;
        int random = (int) (Math.random() * 2);
        if (random == 0) {
            String name = MenNames.values()[(int) (Math.random() * MenNames.values().length)].name();
            child = new Man(name, father.getSurname(), 2018);
        } else {
            String name = WomenNames.values()[(int) (Math.random() * WomenNames.values().length)].name();
            child = new Woman(name, father.getSurname(), 2018);
        }
        child.setIQLevel((father.getIQLevel() + mother.getIQLevel()) / 2);
        child.setFamily(this);
        addChild(child);
        return child;
    }

    public void addChild(Human child) {
        childs.add(child);
        child.setFamily(this);
    }

    public boolean deleteChild(int i) {
        if (i >= childs.size() || i < 0) {
            return false;
        }
        if (childs.get(i) != null) {
            childs.get(i).setFamily(null);
            childs.remove(i);
            return true;
        } else {
            return false;
        }
    }

    public boolean deleteChild(Human child) {
        int initSize = childs.size();
        childs.remove(child);
        int finalSize = childs.size();
        if (initSize == finalSize+1){
            child.setFamily(null);
            return true;
        }
        return false;
    }

    public void addPet (Pet pet){
        pets.add(pet);
    }

    public int countFamily() {
        return 2 + childs.size();
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public List<Human> getChilds() {
        return childs;
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public void setPet(Set<Pet> pets) {
        this.pets = pets;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", childs=" + childs.toString() +
                ", pet=" + pets +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("object deleted: " + this.toString());
    }
}
