package com.alexgrig.homework11;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {


        Pet summerWolf = new DireWolf("Summer");
        summerWolf.setAge(2);
        summerWolf.setTrickLevel(57);
        summerWolf.setHabits("eat meat", "hunt", "growl");

        Human eddardStark = new Man("Eddard", "Stark", 1000);
        HumanSchedule eddardSchedule = new HumanSchedule("praying to Tree", "catch some wildlings or deserters", "judge", "execute the judgment", "manage Winterfell", "family communication, parenting", "family communication, parenting");
        eddardStark.setSchedule(eddardSchedule.getSchedule());
        eddardStark.setIQLevel(50);


        Human catelynStark = new Woman("Catelyn", "Stark", 1010);
        HumanSchedule catelynSchedule = new HumanSchedule("praying to Tree", "manage Winterfell", "husband care", "manage Winterfell", "parenting", "parenting", "parenting");
        catelynStark.setSchedule(catelynSchedule.getSchedule());
        catelynStark.setIQLevel(60);


        HumanSchedule brandonSchedule = new HumanSchedule("climb on roofs", "climb on roofs", "climb on roofs", "climb on roofs", "climb on roofs", "climb on roofs", "fall down");
        Human brandonStark = new Human("Brandon", "Stark", 1037, 70, brandonSchedule.getSchedule());


        Family starks = new Family(catelynStark, eddardStark);
        starks.addPet(summerWolf);
        starks.addChild(brandonStark);


// вызов методов по заданию, проверка корректности:
        System.out.println(brandonStark);
        System.out.println(summerWolf);
        starks.bornChild();
        System.out.println(starks);
//        System.out.println(Arrays.toString(starks.getChilds()));
//        System.out.println(Arrays.toString(brandonStark.getFamily().getChilds()));
    }
}
