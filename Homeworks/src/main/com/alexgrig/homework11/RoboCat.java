package com.alexgrig.homework11;

public class RoboCat extends Pet {
    public RoboCat (){
        super();
    }
    public RoboCat (String nickname){
        super(nickname);
    }
    public RoboCat(String nickname, int age, int trickLevel, String... habits) {
        super(nickname, age, trickLevel, habits);
    }
    {
        setSpecies(Species.ROBO_CAT);
    }

    public void respond() {
        System.out.println("Привет хозяйство! Я твоя новый дружить " + getNickname() + "! Мочь сейчас петь тебе песеня про аккуратно страсть! Ничего извини мой акцент!");
    }
}
