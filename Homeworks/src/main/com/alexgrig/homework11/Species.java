package com.alexgrig.homework11;

public enum Species {
    DOG(false, 4, true), DOMESTIC_CAT(false, 4, true), DIREWOLF(false, 4, true), PARROT(true, 2, false), FISH(false, 0, false), ROBO_CAT(false, 4, false), UNKNOWN(false, 0, false);

    private boolean canFly;
    private int numberOfLegs;
    private boolean hasFur;


    Species(boolean canFly, int numberOfLegs, boolean hasFur) {
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }


    public boolean isCanFly() {
        return canFly;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public boolean isHasFur() {
        return hasFur;
    }
}
