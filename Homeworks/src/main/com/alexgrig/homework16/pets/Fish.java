package com.alexgrig.homework16.pets;

public class Fish extends Pet {
    public Fish (){
        super();
    }
    public Fish (String nickname){
        super(nickname);

    }
    public Fish (String nickname, int age, int trickLevel, String... habits) {
        super(nickname, age, trickLevel, habits);

    }
    {
        setSpecies(Species.FISH);
    }
    public void respond() {
        System.out.println("... ... ... буль-буль");
    }
}
