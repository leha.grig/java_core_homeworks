package com.alexgrig.homework16.console;

public interface Command {
    String get();
    void doCommand();
    default boolean isExit (){return false;}
}
