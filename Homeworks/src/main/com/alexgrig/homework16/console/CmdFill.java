package com.alexgrig.homework16.console;

import com.alexgrig.homework16.Family;
import com.alexgrig.homework16.humans.Human;
import com.alexgrig.homework16.humans.HumanSchedule;
import com.alexgrig.homework16.humans.Man;
import com.alexgrig.homework16.humans.Woman;
import com.alexgrig.homework16.pets.DireWolf;
import com.alexgrig.homework16.pets.Dog;
import com.alexgrig.homework16.pets.DomesticCat;
import com.alexgrig.homework16.pets.Pet;

public class CmdFill implements Command {
    @Override
    public String get() {
        return "Fill";
    }

    @Override
    public void doCommand() {
        Pet summerWolf = new DireWolf("Summer", 2, 57, "eat meat", "hunt", "growl");

        Human eddardStark = new Man("Eddard", "Stark", 1957);
        HumanSchedule eddardSchedule = new HumanSchedule("praying to Tree", "catch some wildlings or deserters", "judge", "execute the judgment", "manage Winterfell", "family communication, parenting", "family communication, parenting");
        eddardStark.setSchedule(eddardSchedule.getSchedule());
        eddardStark.setIQLevel(50);

        Human catelynStark = new Woman("Catelyn", "Stark", 1960);
        HumanSchedule catelynSchedule = new HumanSchedule("praying to Tree", "manage Winterfell", "husband care", "manage Winterfell", "parenting", "parenting", "parenting");
        catelynStark.setSchedule(catelynSchedule.getSchedule());
        catelynStark.setIQLevel(60);

        HumanSchedule brandonSchedule = new HumanSchedule("climb on roofs", "climb on roofs", "climb on roofs", "climb on roofs", "climb on roofs", "climb on roofs", "fall down");
        Human brandonStark = new Human("Brandon", "Stark", 2000, 70, brandonSchedule.getSchedule());

        Family starks = new Family(catelynStark, eddardStark);
        starks.addPet(summerWolf);
        starks.addChild(brandonStark);

        Pet matroskin = new DomesticCat("Matroskin", 5, 75, "sew", "care about cow");
        Pet sharik = new Dog("Sharik", 3, 25);

        Human papaFedora = new Man("Papa", "F", 1945);
        Human mamaFedora = new Woman("Mama", "F", 1950);
        Human dyadyaFedor = new Man("Fedor", "Dydya", 1970);
        Human masha = new Woman("Maria", "Prosto", 1980);
        Human pedro = new Man("Pedro", "Don", 1980);

        Family fedors = new Family(mamaFedora, papaFedora);
        fedors.addChild(dyadyaFedor);
        fedors.addPet(matroskin);
        fedors.addPet(sharik);
        Family pedry = new Family(masha, pedro);
    }
}
