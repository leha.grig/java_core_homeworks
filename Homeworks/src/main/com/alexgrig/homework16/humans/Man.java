package com.alexgrig.homework16.humans;

import com.alexgrig.homework16.Family;
import com.alexgrig.homework16.pets.Pet;

import java.util.Map;

public final class Man extends Human {
    public Man (){
        super();
    }
    public Man (String name, String surname, int yearOfBirth){
        super(name, surname, yearOfBirth);
    }
    public Man(String name, String surname, String yearOfBirth) {
        super(name, surname, yearOfBirth);
    }
    public Man(String name, String surname, String yearOfBirth, int IQLevel) {
        super(name, surname, yearOfBirth, IQLevel);
    }
    public Man (String name, String surname, int yearOfBirth, int IQLevel, Map<String, String> schedule){
        super(name, surname, yearOfBirth, IQLevel, schedule);
    }
    public Man (String name, String surname, String yearOfBirth, int IQLevel, Map<String, String> schedule){
        super(name, surname, yearOfBirth, IQLevel, schedule);
    }
    public Man (String name, String surname, String yearOfBirth, int IQLevel, Map<String, String> schedule, Family family) {
        super(name, surname, yearOfBirth, IQLevel, schedule, family);
    }
    public void searchSocks () {
        System.out.println("Пойду опять в разных, все равно в ботинках");
    }

    @Override
    public void greetPet(Pet pet) {
        if (getFamily().getPets().contains(pet)) {
        System.out.println("Привет, " + pet.getNickname() + ". Я тоже рад тебя видеть");
        }
    }

}
