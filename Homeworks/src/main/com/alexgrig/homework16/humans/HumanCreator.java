package com.alexgrig.homework16.humans;

public interface HumanCreator {
    Human bornChild();
}
