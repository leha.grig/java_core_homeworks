package com.alexgrig.homework16.humans;

import com.alexgrig.homework16.Family;
import com.alexgrig.homework16.pets.Pet;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

public class Human {
    private static final Random RANDOM = new Random();
    private String name;
    private String surname;
    private long birthDate;
    private int IQLevel;
    private Map<String, String> schedule;
    private Family family;

    /*static {
        System.out.println("The new class Human is loading");
    }

    {
        System.out.println("The new object Human is creating");
    }*/

    public Human(String name, String surname, String birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = stringToInstantConverter(birthDate).toEpochMilli();
    }

    public Human(String name, String surname, int yearOfBirth) {
        this.name = name;
        this.surname = surname;
        this.birthDate = stringToInstantConverter("01/01/" + yearOfBirth).toEpochMilli();
    }

    public Human(String name, String surname, String birthDate, int IQLevel, Map<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = stringToInstantConverter(birthDate).toEpochMilli();
        this.IQLevel = IQLevel;
        this.schedule = schedule;
    }

    public Human(String name, String surname, int yearOfBirth, int IQLevel, Map<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = stringToInstantConverter("01/01/" + yearOfBirth).toEpochMilli();
        this.IQLevel = IQLevel;
        this.schedule = schedule;
    }

    public Human(String name, String surname, String birthDate, int IQLevel, Map<String, String> schedule, Family family) {
        this.name = name;
        this.surname = surname;
        this.birthDate = stringToInstantConverter(birthDate).toEpochMilli();
        this.IQLevel = IQLevel;
        this.schedule = schedule;
        this.family = family;
    }

    public Human(String name, String surname, String birthDate, int IQLevel) {
        this.name = name;
        this.surname = surname;
        this.birthDate = stringToInstantConverter(birthDate).toEpochMilli();
        this.IQLevel = IQLevel;
    }

    public Human() {
    }

    private Instant stringToInstantConverter(String string) {
        if (string.matches("\\d{2}[-./]\\d{2}[-./]\\d{4}")) {
            string = string.replaceAll("[-.]", "/");
            LocalDateTime ldt = LocalDate.parse(string, DateTimeFormatter.ofPattern("dd/MM/yyyy")).atStartOfDay();
            ZonedDateTime zdt = ldt.atZone(ZoneId.systemDefault());
            return zdt.toInstant();
        }
        return Instant.now();
    }

    public String describeAge() {

        LocalDate now = LocalDate.now();
        LocalDate ldtBirthDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(birthDate), ZoneId.systemDefault())
                .toLocalDate();

        Period period = Period.between(ldtBirthDate, now);
        String yearStr = period.getYears() > 4 || period.getYears() == 0 ? " лет " : period.getYears() != 1 ? " года " : " год ";
        String monthsStr = period.getMonths() > 4 || period.getMonths() == 0 ? " месяцев " : period.getMonths() != 1 ? " месяца " : " месяц ";
        String daysStr = period.getDays() > 4 || period.getDays() == 0 ? " дней." : period.getDays() != 1 ? " дня." : " день.";

        return "Сегодня " + name + " " + surname + ": " + period.getYears() + yearStr + period.getMonths() + monthsStr + period.getDays() + daysStr;
    }

    public int getYearOfBirth() {
        LocalDate ldtBirthDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(birthDate), ZoneId.systemDefault())
                .toLocalDate();

        return ldtBirthDate.getYear();
    }

    public void greetPet(Pet pet) {
        if (family.getPets().contains(pet)) {
            System.out.println("Привет, " + pet.getNickname());
        }
    }

    public void describePet(Pet pet) {
        if (family.getPets().contains(pet)) {
            String tricky = pet.getTrickLevel() <= 50 ? "почти не хитрый" : "очень хитрый";
            String yearsName = pet.getAge() <= 4 ? "года" : "лет";
            if (pet.getAge() == 1) {
                yearsName = "год";
            }
            System.out.println("У меня есть " + pet.getSpecies() + ", ему " + pet.getAge() + " " + yearsName + ", он " + tricky);
        }
    }

    public Boolean feedPet(Boolean bool, Pet pet) {
        if (family.getPets().contains(pet)) {
            String feedingYes = "Хм... покормлю-ка я " + pet.getNickname();
            String feedingNo = "Думаю, " + pet.getNickname() + " не голоден.";
            if (bool) {
                System.out.println(feedingYes);
                return true;
            } else {
                int temp = RANDOM.nextInt(101);
                if (temp < pet.getTrickLevel()) {
                    System.out.println(feedingYes);
                    return true;
                }
                System.out.println(feedingNo);
                return false;
            }
        }
        return false;
    }

    public String getSubclass (){
        if(this instanceof Man){
            return "Man";
        }
        if(this instanceof Woman){
            return "Woman";
        }
        return "Human";
    }

    public String toString(){
        return String.format("{name='%s', surname='%s', birthdate='%s', iq='%d', schedule=%s}%n", getName(), getSurname(), getBirthDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")), getIQLevel(), getSchedule());
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public LocalDate getBirthDate() {
        LocalDate ldtBirthDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(birthDate), ZoneId.systemDefault())
                .toLocalDate();
        return ldtBirthDate;
    }

    public int getIQLevel() {
        return IQLevel;
    }

    public void setIQLevel(int IQLevel) {
        this.IQLevel = IQLevel;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Human getMother() {
        return this.getFamily().getMother();
    }

    public Human getFather() {
        return this.getFamily().getFather();
    }


    /*@Override
    public String toString() {
        String ldtBirthDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(birthDate), ZoneId.systemDefault())
                .format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        return '{' + name + ' ' +
                surname +
                ", birthDate=" + ldtBirthDate +
                ", IQLevel=" + IQLevel +
                ", schedule=" + schedule +
                '}';
    }*/

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Human human = (Human) obj;
        return birthDate == human.birthDate &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate) * 7;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString());
    }
}
