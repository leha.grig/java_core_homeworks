package com.alexgrig.homework16;

import com.alexgrig.homework16.humans.Human;
import com.alexgrig.homework16.humans.Man;
import com.alexgrig.homework16.humans.Woman;
import com.alexgrig.homework16.pets.Pet;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService {
    private FamilyDao familyDao;

    public FamilyService() {
    }

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public void setFamilyDao(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        getAllFamilies().stream()
                .forEach(elem -> System.out.println("Family" + (getAllFamilies().indexOf(elem) + 1) + ": " +
                        elem.getFather().getName() + " " + elem.getFather().getSurname() + ", "
                        + elem.getMother().getName() + " " + elem.getMother().getSurname() + ", "
                        + elem.printChilds()));
    }

    public List<Family> getFamiliesBiggerThan(int members) {
        List<Family> result = getAllFamilies().stream()
                .filter(elem -> elem.countFamily() > members)
                .collect(Collectors.toList());
        System.out.println(result);
        return result;
    }

    public List<Family> getFamiliesLessThan(int members) {
        List<Family> result = getAllFamilies().stream()
                .filter(elem -> elem.countFamily() < members)
                .collect(Collectors.toList());
        System.out.println(result);
        return result;
    }

    public int countFamiliesWithMemberNumber(int members) {
        return (int) getAllFamilies().stream()
                .filter(elem -> elem.countFamily() == members)
                .count();
    }

    public Family createNewFamily(Human woman, Human man) {

        // проверка аргументов на null
        if (woman == null || man == null) {
            return null;
        }

        // проверка правильной последовательности мужчины и женщины. Если наоборот - смена их местами
        if (woman.getClass().getSimpleName().equals("Man") && man.getClass().getSimpleName().equals("Woman")) {
            Human temp = woman;
            woman = man;
            man = temp;
        }

        // проверка на создание уже существующей семьи. Внесение в базу и возврат существующей семьи в случае успеха
        if (woman.getFamily() != null && woman.getFamily().getMother().equals(woman) && woman.getFamily().equals(man.getFamily()) && man.getFamily().getFather().equals(man)) {
            familyDao.saveFamily(woman.getFamily());
            return woman.getFamily();
        }

        // если новые члены семьи уже были основой другой семьи - удаление прежней семьи из базы
        if (woman.getFamily() != null && woman.getFamily().getMother().equals(woman)) {
            familyDao.deleteFamily(woman.getFamily());
        }

        // если новые члены семьи уже были основой другой семьи - удаление прежней семьи из базы
        if (man.getFamily() != null && man.getFamily().getFather().equals(man)) {
            familyDao.deleteFamily(man.getFamily());
        }

        Family family = new Family(woman, man);

        familyDao.saveFamily(family);
        return family;
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family, String manName, String womanName) {

        //выход из метода при некорректных аргументах или их отсутствии
        if (family == null || manName == null || !manName.matches("[A-Za-z]{1,15}-?[A-Za-z]{1,15}") || womanName == null || !womanName.matches("[A-Za-z]{1,15}-?[A-Za-z]{1,15}")) {
            return family;
        }
        Human child;
        int year = LocalDate.now().getYear();
        int random = (int) (Math.random() * 2);
        if (random == 0) {
            child = new Man(manName, family.getFather().getSurname(), year);
        } else {
            child = new Woman(womanName, family.getFather().getSurname(), year);
        }
        child.setIQLevel((family.getFather().getIQLevel() + family.getMother().getIQLevel()) / 2);
        child.setFamily(family);
        family.addChild(child);
        familyDao.saveFamily(family);
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        if (family == null || child == null) {
            return family;
        }
        if (family.getChilds().contains(child)) {
            return family;
        }
        child.setFamily(family);
        family.addChild(child);
        familyDao.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        int currentYear = LocalDate.now().getYear();
        getAllFamilies().forEach(f -> {
            List<Human> found = f.getChilds().stream()
                    .filter(ch -> (currentYear - ch.getYearOfBirth()) > age)
                    .collect(Collectors.toList());
            f.getChilds().removeAll(found);
            familyDao.saveFamily(f);
        });
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int familyIndex) {
        return familyDao.getFamilyByIndex(familyIndex).getPets();
    }

    public void addPet(int familyIndex, Pet pet) {
        if (pet != null) {
            Family family = familyDao.getFamilyByIndex(familyIndex);
            if (family != null) {
                family.addPet(pet);
                familyDao.saveFamily(family);
            }
        }
    }
}
