package com.alexgrig.extra;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

// задача: получить ряд (заданное количество) случайных уникальных чисел в заданном диапазоне
public class GetUniqueEvenInts {

    private void capacityCheck(int capacity, int iniValue, int finalValue) {
        if ((finalValue - iniValue) / 2 < capacity) {
            throw new RuntimeException("Capacity must not be lower than half of the range!");
        }
    }

    public Set<Integer> addNumbersHashSet(int capacity, int iniValue, int finalValue) {
        capacityCheck(capacity, iniValue, finalValue);
        Set<Integer> set = new HashSet<>();
        while (set.size() < capacity) {
            set.add(((int) (Math.random() * (finalValue - iniValue) / 2 + 1)) * 2);
        }
        return set;
    }

    public List<Integer> addNumbersArrayList(int capacity, int iniValue, int finalValue) {
        capacityCheck(capacity, iniValue, finalValue);
        List<Integer> temp = new ArrayList<>();
        if(iniValue % 2 > 0){
            iniValue++;
        }
        for (int i = iniValue; i <= finalValue/2; i += 2) {
            temp.add(i);
        }
        Collections.shuffle(temp);
        return new ArrayList(temp.subList(0, capacity));
    }

    public List<Integer> addNumbersThreadLocalRandom(int capacity, int iniValue, int finalValue) {
        capacityCheck(capacity, iniValue, finalValue);
        return ThreadLocalRandom.current().ints(iniValue, finalValue).filter(i->i%2==0).distinct().limit(capacity).boxed()
                .collect(Collectors.toCollection(ArrayList::new));
    }
}

class App {
    public static void main(String[] args) {
        GetUniqueEvenInts getUniqEvenInts = new GetUniqueEvenInts();
        System.out.println(getUniqEvenInts.addNumbersHashSet(100, 0, 1000));
        System.out.println(getUniqEvenInts.addNumbersArrayList(100, 0, 1000));
        System.out.println(getUniqEvenInts.addNumbersThreadLocalRandom(100, 0, 1000));
    }
}