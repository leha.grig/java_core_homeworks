package com.alexgrig.homework14;

import com.alexgrig.homework14.humans.Human;
import com.alexgrig.homework14.humans.HumanSchedule;
import com.alexgrig.homework14.humans.Man;
import com.alexgrig.homework14.humans.Woman;
import com.alexgrig.homework14.pets.DireWolf;
import com.alexgrig.homework14.pets.Dog;
import com.alexgrig.homework14.pets.DomesticCat;
import com.alexgrig.homework14.pets.Pet;

public class Main {
    public static void main(String[] args) {


        Pet summerWolf = new DireWolf("Summer");
        summerWolf.setAge(2);
        summerWolf.setTrickLevel(57);
        summerWolf.setHabits("eat meat", "hunt", "growl");

        Human eddardStark = new Man("Eddard", "Stark", 1000);
        HumanSchedule eddardSchedule = new HumanSchedule("praying to Tree", "catch some wildlings or deserters", "judge", "execute the judgment", "manage Winterfell", "family communication, parenting", "family communication, parenting");
        eddardStark.setSchedule(eddardSchedule.getSchedule());
        eddardStark.setIQLevel(50);


        Human catelynStark = new Woman("Catelyn", "Stark", 1010);
        HumanSchedule catelynSchedule = new HumanSchedule("praying to Tree", "manage Winterfell", "husband care", "manage Winterfell", "parenting", "parenting", "parenting");
        catelynStark.setSchedule(catelynSchedule.getSchedule());
        catelynStark.setIQLevel(60);


        HumanSchedule brandonSchedule = new HumanSchedule("climb on roofs", "climb on roofs", "climb on roofs", "climb on roofs", "climb on roofs", "climb on roofs", "fall down");
        Human brandonStark = new Human("Brandon", "Stark", 1037, 70, brandonSchedule.getSchedule());


        Family starks = new Family(catelynStark, eddardStark);
        starks.addPet(summerWolf);
        starks.addChild(brandonStark);

        Pet matroskin = new DomesticCat("Matroskin", 5, 75, "sew", "care about cow");
        Pet sharik = new Dog();

        Human papaFedora = new Man("Papa", "F", 1945);
        Human mamaFedora = new Woman("Mama", "F", 1950);
        Human dyadyaFedor = new Man("Fedor", "Dydya", 1970);
        Human masha = new Woman("Maria", "Prosto", 1980);
        Human pedro = new Man("Pedro", "Don", 1980);

        Family fedors = new Family(mamaFedora, papaFedora);
        fedors.addPet(matroskin);
//        fedors.addChild(dyadyaFedor);

        FamilyDao newFamilyDao = new CollectionFamilyDao(starks, fedors);
        FamilyService familyService = new FamilyService(newFamilyDao);
        FamilyController familyController = new FamilyController(familyService);

// вызов методов по заданию, проверка корректности:
        Human gosha = new Man("Gosha", "Parrot", "03-11.2001", 50);
        System.out.println(brandonStark.describeAge());

        System.out.println(gosha.describeAge());
        System.out.println(brandonStark);
    }
}
