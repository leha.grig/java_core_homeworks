package com.alexgrig.homework14.pets;

public class Dog extends Pet implements PetFouling {
    public Dog (){
        super();
    }
    public Dog (String nickname){
        super(nickname);

    }
    public Dog(String nickname, int age, int trickLevel, String... habits) {
        super(nickname, age, trickLevel, habits);

    }
    {
        setSpecies(Species.DOG);
    }
    public void foul()  {
        System.out.println("Нужно хорошо замести следы...");
    }
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + getNickname() + " Я соскучился!");
    }
}
