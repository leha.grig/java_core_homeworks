package com.alexgrig.homework14.pets;

public class DomesticCat extends Pet implements PetFouling {
    public DomesticCat (){
        super();
    }
    public DomesticCat (String nickname){
        super(nickname);

    }
    public DomesticCat (String nickname, int age, int trickLevel, String... habits) {
        super(nickname, age, trickLevel, habits);

    }
    {
        setSpecies(Species.DOMESTIC_CAT);
    }
    public void foul()  {
        System.out.println("Нужно хорошо замести следы...");
    }
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + getNickname() + " Я соскучился!");
    }
}
