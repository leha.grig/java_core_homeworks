package com.alexgrig.homework14.humans;

public interface HumanCreator {
    Human bornChild();
}
