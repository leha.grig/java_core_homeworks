package com.alexgrig.homework12;

import java.util.Map;

public final class Man extends Human {
    public Man (){
        super();
    }
    public Man (String name, String surname, int yearOfBirth){
        super(name, surname, yearOfBirth);
    }
    public Man (String name, String surname, int yearOfBirth, int IQLevel, Map<String, String> schedule){
        super(name, surname, yearOfBirth, IQLevel, schedule);
    }
    public Man (String name, String surname, int yearOfBirth, int IQLevel, Map<String, String> schedule, Family family) {
        super(name, surname, yearOfBirth, IQLevel, schedule, family);
    }
    public void searchSocks () {
        System.out.println("Пойду опять в разных, все равно в ботинках");
    }

    @Override
    public void greetPet(Pet pet) {
        if (getFamily().getPets().contains(pet)) {
        System.out.println("Привет, " + pet.getNickname() + ". Я тоже рад тебя видеть");
        }
    }
}
