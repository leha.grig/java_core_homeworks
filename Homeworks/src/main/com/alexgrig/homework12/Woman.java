package com.alexgrig.homework12;

import java.util.Map;

public final class Woman extends Human {
    public Woman() {
        super();
    }

    public Woman(String name, String surname, int yearOfBirth) {
        super(name, surname, yearOfBirth);
    }

    public Woman(String name, String surname, int yearOfBirth, int IQLevel, Map<String, String> schedule) {
        super(name, surname, yearOfBirth, IQLevel, schedule);
    }

    public Woman(String name, String surname, int yearOfBirth, int IQLevel, Map<String, String> schedule, Family family) {
        super(name, surname, yearOfBirth, IQLevel, schedule, family);
    }

    public void phoneTalk(int numOfCicles, int delayMs) {
        int count = 0;
        while (count < numOfCicles) {
            System.out.print("Ага... Ммм... А он?... А ты?... А он?... А ты?...");
            try {
                Thread.sleep(delayMs);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            System.out.print("Да ты что!!!... Ну да? ... ");
            count++;
        }
    }

    @Override
    public void greetPet(Pet pet) {
        if (getFamily().getPets().contains(pet)) {
            System.out.println("Привет, " + pet.getNickname() + ". Проголодался? Сейчас мамочка тебя накормит");
        }
    }
}
