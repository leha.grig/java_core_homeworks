package com.alexgrig.homework12;

import java.util.Map;
import java.util.Objects;
import java.util.Random;

public class Human {
    private static final Random RANDOM = new Random();
    private String name;
    private String surname;
    private int yearOfBirth;
    private int IQLevel;
    private Map<String, String> schedule;
    private Family family;

    static {
        System.out.println("The new class Human is loading");
    }

    {
        System.out.println("The new object Human is creating");
    }

    public Human(String name, String surname, int yearOfBirth) {
        this.name = name;
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
    }

    public Human(String name, String surname, int yearOfBirth, int IQLevel, Map<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
        this.IQLevel = IQLevel;
        this.schedule = schedule;
    }

    public Human(String name, String surname, int yearOfBirth, int IQLevel, Map<String, String> schedule, Family family) {
        this.name = name;
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
        this.IQLevel = IQLevel;
        this.schedule = schedule;
        this.family = family;
    }

    public Human() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public int getIQLevel() {
        return IQLevel;
    }

    public void setIQLevel(int IQLevel) {
        this.IQLevel = IQLevel;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Human getMother() {
        return this.getFamily().getMother();
    }

    public Human getFather() {
        return this.getFamily().getFather();
    }

    public void greetPet(Pet pet) {
        if (family.getPets().contains(pet)) {
            System.out.println("Привет, " + pet.getNickname());
        }
    }

    public void describePet(Pet pet) {
        if (family.getPets().contains(pet)) {
            String tricky = pet.getTrickLevel() <= 50 ? "почти не хитрый" : "очень хитрый";
            String yearsName = pet.getAge() <= 4 ? "года" : "лет";
            if (pet.getAge() == 1) {
                yearsName = "год";
            }
            System.out.println("У меня есть " + pet.getSpecies() + ", ему " + pet.getAge() + " " + yearsName + ", он " + tricky);
        }
    }

    public Boolean feedPet(Boolean bool, Pet pet) {
        if (family.getPets().contains(pet)) {
            String feedingYes = "Хм... покормлю-ка я " + pet.getNickname();
            String feedingNo = "Думаю, " + pet.getNickname() + " не голоден.";
            if (bool) {
                System.out.println(feedingYes);
                return true;
            } else {
                int temp = RANDOM.nextInt(101);
                if (temp < pet.getTrickLevel()) {
                    System.out.println(feedingYes);
                    return true;
                }
                System.out.println(feedingNo);
                return false;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", yearOfBirth=" + yearOfBirth +
                ", IQLevel=" + IQLevel +
                ", schedule=" + schedule +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Human human = (Human) obj;
        return yearOfBirth == human.yearOfBirth &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, yearOfBirth) * 7;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString());
    }
}
