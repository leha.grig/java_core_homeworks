package com.alexgrig.homework1;

import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;

public class YearsGame {
    private String playerName;
    private int[] yearsEntered = new int[]{};
    private int theDate;
    private int theLine;
    private final String[][] YEARS;
    private final Random RANDOM = new Random();
    private final Scanner SCAN = new Scanner(System.in);


    public YearsGame(String[][] years) {
        this.YEARS = years;
    }

    public void setPlayerName() {
        System.out.println("Enter your name");
        this.playerName = SCAN.nextLine();
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setTheDate() {
        this.theLine = RANDOM.nextInt(YEARS.length);
        this.theDate = Integer.valueOf(YEARS[theLine][1]);
    }

    public void startTheGame() {
        System.out.println("Let the game begin!");
        System.out.println(YEARS[theLine][0]);
        while (true) {

            String yearString = SCAN.nextLine();
            yearString = checkTheNumber(yearString);

            int year = Integer.valueOf(yearString); // или так: Integer.parseInt(yearString)
            yearsEntered = Arrays.copyOf(yearsEntered, yearsEntered.length + 1);
            yearsEntered[yearsEntered.length - 1] = year;
            if (year < theDate) {
                System.out.println("The year is too small. Please, try again.");
            }
            if (year > theDate) {
                System.out.println("The year is too big. Please, try again.");
            }
            if (year == theDate) {
                System.out.println("Congratulations, " + playerName + "!");
                Arrays.sort(yearsEntered);
                System.out.println("Your years:" + Arrays.toString(yearsEntered));
                break;
            }
        }
    }

    public String checkTheNumber(String str) {
        // проверка на число с RegEx (если встречается хотя бы одно нечисло в строке):
        // ".*\\D+.*" Тут .* - любое количество любых символов, \D+ - один или больше символов-нечисел.
        // Еще вариант записи того же: yearString.matches(".*[^0-9].*")
        while (str.matches(".*\\D+.*")) {
            System.out.println("That not a number! Please, try again.");
            str = SCAN.nextLine();
        }
        return str;
    }
}
