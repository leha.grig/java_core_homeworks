package com.alexgrig.homework1;

import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;

public class NumbersGame {
    private String playerName;
    private int[] numbersEntered = new int[]{};
    private int theNumber;
    private final Random RANDOM = new Random();
    private final Scanner SCAN = new Scanner(System.in);


    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName() {

        System.out.println("Enter your name");
        this.playerName = SCAN.nextLine();
    }

    public void setTheNumber() {
        this.theNumber = RANDOM.nextInt(101);
    }

    public void startTheGame() {

        System.out.println("Let the game begin!");
        System.out.println("Enter your number: ");
        while (true) {
            while (!SCAN.hasNextInt()) { // Проверка на число, встроенный метод Scanner'a
                System.out.println("That not a number! Please, try again.");
                SCAN.next(); // без этой строчки (сдвиг сканера дальше) войдет в бесконечный цикл
            }
            int num = SCAN.nextInt();
            numbersEntered = Arrays.copyOf(numbersEntered, numbersEntered.length + 1);
            numbersEntered[numbersEntered.length - 1] = num;
            if (num < theNumber) {
                System.out.println("Your number is too small. Please, try again.");
            }
            if (num > theNumber) {
                System.out.println("Your number is too big. Please, try again.");
            }
            if (num == theNumber) {
                System.out.println("Congratulations, " + playerName + "!");
                Arrays.sort(numbersEntered);
                System.out.println("Your numbers:" + Arrays.toString(numbersEntered));
                break;
            }
        }
    }
}
