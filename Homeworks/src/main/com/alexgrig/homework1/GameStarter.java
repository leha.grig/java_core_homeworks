package com.alexgrig.homework1;

public class GameStarter {
    public static void main(String[] args) {

        // start the NumbersGame:

        /*NumbersGame newGame = new NumbersGame();
        newGame.setPlayerName();
        newGame.setTheNumber();
        newGame.startTheGame();*/


        // start the YearsGame:

        String [][] years = {
                {"What is the year of X century beginning?", "900"},
                {"What is the year of XI century beginning?", "1000"},
                {"What is the year of XII century beginning?", "1100"},
                {"What is the year of XIII century beginning?", "1200"},
                {"What is the year of XIV century beginning?", "1300"},
                {"What is the year of XV century beginning?", "1400"},
                {"What is the year of XVI century beginning?", "1500"},
                {"What is the year of XVII century beginning?", "1600"},
                {"What is the year of XVIII century beginning?", "1700"},
                {"What is the year of XIX century beginning?", "1800"},
                {"What is the year of XX century beginning?", "1900"},
                {"What is the year of XXI century beginning?", "2000"},
        };

        YearsGame newGame = new YearsGame(years);
        newGame.setPlayerName();
        newGame.setTheDate();
        newGame.startTheGame();
    }
}
