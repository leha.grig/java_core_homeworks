package com.alexgrig.homework15;

import com.alexgrig.homework15.humans.Human;
import com.alexgrig.homework15.pets.Pet;

import java.util.List;
import java.util.Set;

public class FamilyController {
    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }
    public FamilyController (){}

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int members) {
        return familyService.getFamiliesBiggerThan(members);
    }

    public List<Family> getFamiliesLessThan(int members) {
        return familyService.getFamiliesLessThan(members);
    }

    public int countFamiliesWithMemberNumber(int members) {
        return familyService.countFamiliesWithMemberNumber(members);
    }

    public Family createNewFamily(Human woman, Human man) {
        return familyService.createNewFamily(woman, man);
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family, String manName, String womanName) {
        return familyService.bornChild(family, manName, womanName);
    }

    public Family adoptChild(Family family, Human child) {
        return familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyService.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    public Set<Pet> getPets(int familyIndex) {
        return familyService.getPets(familyIndex);
    }

    public void addPet(int familyIndex, Pet pet) {
        familyService.addPet(familyIndex, pet);
    }
}
