package com.alexgrig.homework15.humans;

public interface HumanCreator {
    Human bornChild();
}
