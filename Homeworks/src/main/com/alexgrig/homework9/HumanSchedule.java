package com.alexgrig.homework9;

import java.util.Arrays;

public class HumanSchedule {
    private final String[][] schedule;

    public HumanSchedule(String suTask, String moTask, String tuTask, String weTask, String thTask, String frTask, String saTask){
        schedule = new String[7][2];
        schedule[0][0] = DayOfWeek.SUNDAY.name();
        schedule[0][1] = suTask;
        schedule[1][0] = DayOfWeek.MONDAY.name();
        schedule[1][1] = moTask;
        schedule[2][0] = DayOfWeek.TUESDAY.name();
        schedule[2][1] = tuTask;
        schedule[3][0] = DayOfWeek.WEDNESDAY.name();
        schedule[3][1] = weTask;
        schedule[4][0] = DayOfWeek.THURSDAY.name();
        schedule[4][1] = thTask;
        schedule[5][0] = DayOfWeek.FRIDAY.name();
        schedule[5][1] = frTask;
        schedule[6][0] = DayOfWeek.SATURDAY.name();
        schedule[6][1] = saTask;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    @Override
    public String toString() {
        return "HumanSchedule{" +
                "schedule=" + Arrays.deepToString(schedule) +
                '}';
    }
}
