package com.alexgrig.homework7;

public class Main {
    public static void main(String[] args) {


        Pet summerWolf = new Pet("direwolf", "Summer");
        summerWolf.setAge(2);
        summerWolf.setTrickLevel(57);
        summerWolf.setHabits("eat meat", "hunt", "growl");

        Human eddardStark = new Human("Eddard", "Stark", 1000);
        HumanSchedule eddardSchedule = new HumanSchedule("praying to Tree", "catch some wildlings or deserters", "judge", "execute the judgment", "manage Winterfell", "family communication, parenting", "family communication, parenting");
        eddardStark.setSchedule(eddardSchedule.getSchedule());
        eddardStark.setIQLevel(50);


        Human catelynStark = new Human("Catelyn", "Stark", 1010);
        HumanSchedule catelynSchedule = new HumanSchedule("praying to Tree", "manage Winterfell", "husband care", "manage Winterfell", "parenting", "parenting", "parenting");
        catelynStark.setSchedule(catelynSchedule.getSchedule());
        catelynStark.setIQLevel(60);


        HumanSchedule brandonSchedule = new HumanSchedule("climb on roofs", "climb on roofs", "climb on roofs", "climb on roofs", "climb on roofs", "climb on roofs", "fall down");
        Human brandonStark = new Human("Brandon", "Stark", 1037, 70, brandonSchedule.getSchedule());


        Pet matroskin = new Pet("cat", "Matroskin", 5, 75, "sew", "care about cow");
        Pet sharik = new Pet();

        Human papaFedora = new Human();
        Human mamaFedora = new Human();
        Human dyadyaFedor = new Human ("Fedor", "Dydya", 1970);

        Family starks = new Family (catelynStark, eddardStark);
        starks.setPet(summerWolf);
        starks.addChild(brandonStark);
        starks.deleteChild(brandonStark);

        Family fedors = new Family (mamaFedora, papaFedora);
        fedors.setPet(matroskin);
        fedors.addChild(dyadyaFedor);

        // вызов методов по заданию:

//        System.out.println(eddardStark);
//        System.out.println(catelynStark);
//        System.out.println(brandonStark);
//        System.out.println(papaFedora);
//        System.out.println(mamaFedora);
//        System.out.println(dyadyaFedor);
        System.out.println(starks);
        System.out.println(eddardStark.getFamily());
        System.out.println(brandonStark.getFamily());
    }
}
