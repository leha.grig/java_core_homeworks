package com.alexgrig.homework7;

import java.util.Arrays;
import java.util.Objects;

public class Pet {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    static {
        System.out.println("The new class Pet is loading");
    }

    {
        System.out.println("The new object Pet is creating");
    }

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    Pet(String species, String nickname, int age, int trickLevel, String... habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    Pet() {
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    void eat() {
        System.out.println("Я кушаю");
    }

    void respond() {
        System.out.println("Привет, хозяин. Я - " + nickname + " Я соскучился!");
    }

    void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    void setAge(int age) {
        this.age = age;
    }

    void setTrickLevel(int level) {
        this.trickLevel = level;
    }

    void setHabits(String... habits) {
        this.habits = habits;
    }

    String getSpecies() {
        return this.species;
    }

    String getNickname() {
        return this.nickname;
    }

    int getAge() {
        return this.age;
    }

    int getTrickLevel() {
        return this.trickLevel;
    }

    String[] getHabits() {
        return this.habits;
    }

    @Override
    public String toString() {
        String habitsExist = "no habits";
        if (this.getHabits() != null) {
            habitsExist = Arrays.toString(this.getHabits());
        }
        return this.getSpecies() + "{nickname='" + this.getNickname() + "', age=" + this.getAge() + ", trickLevel=" + this.getTrickLevel() + ", habits=" + habitsExist + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return trickLevel == pet.trickLevel &&
                Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, trickLevel) * 11;
    }
}
