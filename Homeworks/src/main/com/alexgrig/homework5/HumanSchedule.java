package com.alexgrig.homework5;

public class HumanSchedule {
    private final String[][] schedule;

    public HumanSchedule(String suTask, String moTask, String tuTask, String weTask, String thTask, String frTask, String saTask){
        schedule = new String[7][2];
        schedule[0][0] = "Sunday";
        schedule[0][1] = suTask;
        schedule[1][0] = "Monday";
        schedule[1][1] = moTask;
        schedule[2][0] = "Tuesday";
        schedule[2][1] = tuTask;
        schedule[3][0] = "Wednesday";
        schedule[3][1] = weTask;
        schedule[4][0] = "Thursday";
        schedule[4][1] = thTask;
        schedule[5][0] = "Friday";
        schedule[5][1] = frTask;
        schedule[6][0] = "Saturday";
        schedule[6][1] = saTask;
    }

    public String[][] getSchedule() {
        return schedule;
    }
}
