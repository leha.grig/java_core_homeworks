package com.alexgrig.homework5;

import java.util.Arrays;
import java.util.Random;

public class Human {
    private static final Random RANDOM = new Random();
    private String name;
    private String surname;
    private int yearOfBirth;
    private Human mother;
    private Human father;
    private int IQLevel;
    private String[][] schedule;
    private Pet pet;

    public Human(String name, String surname, int year_of_birth) {
        this.name = name;
        this.surname = surname;
        yearOfBirth = year_of_birth;
    }

    public Human(String name, String surname, int yearOfBirth, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
        this.mother = mother;
        this.father = father;
    }

    public Human(String name, String surname, int yearOfBirth, Human mother, Human father, int IQLevel, String[][] schedule, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
        this.mother = mother;
        this.father = father;
        this.IQLevel = IQLevel;
        this.schedule = schedule;
        this.pet = pet;
    }

    //если ты объект хранишь в БД, сериализируешь/десериализируешь и передаешь и принимаешь на каких то эндпоинтах,
    // то тебе прийдется создавать конструктор по умолчанию (пустой)
    //и работать с геттерами/сеттерами (не создавать final полей)
    public Human() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public int getIQLevel() {
        return IQLevel;
    }

    public void setIQLevel(int IQLevel) {
        this.IQLevel = IQLevel;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void greetPet() {
        System.out.println("Привет, " + pet.getNickname());
    }

    public void describePet() {
        String tricky = pet.getTrickLevel() <= 50 ? "почти не хитрый" : "очень хитрый";
        String yearsName = pet.getAge() <= 4 ? "года" : "лет";
        if (pet.getAge() == 1) {
            yearsName = "год";
        }
        System.out.println("У меня есть " + pet.getSpecies() + ", ему " + pet.getAge() + " " + yearsName + ", он " + tricky);
    }

    public Boolean feedPet(Boolean bool) {
        String feedingYes = "Хм... покормлю-ка я " + pet.getNickname();
        String feedingNo = "Думаю, " + pet.getNickname() + " не голоден.";
        if (bool) {
            System.out.println(feedingYes);
            return true;
        } else {
            int temp = RANDOM.nextInt(101);
            if (temp < pet.getTrickLevel()) {
                System.out.println(feedingYes);
                return true;
            }
            System.out.println(feedingNo);
            return false;
        }
    }

    @Override
    public String toString() {
        String petExist = "has no pet";
        if (this.getPet() != null) {
            String habitsExist = "no habbits";
            if (this.getPet().getHabits() != null) {
                habitsExist = Arrays.toString(this.getPet().getHabits());
            }
            petExist = "pet=" + this.getPet().getSpecies() + "{nickname='" + this.getPet().getNickname() + "', age=" + this.getPet().getAge() + ", trickLevel=" + this.getPet().getTrickLevel() + ", habits=" + habitsExist + "}";
        }
        String motherExistName = null;
        String motherExistSurname = null;
        String fatherExistName = null;
        String fatherExistSurname = null;

        if (this.getMother() != null){
            motherExistName = this.getMother().getName();
            motherExistSurname = this.getMother().getSurname();
        }
        if (this.getFather() != null){
            fatherExistName = this.getFather().getName();
            fatherExistSurname = this.getFather().getSurname();
        }
        return "Human{name='" + this.getName() + "', surname='" + this.getSurname() + "', year=" + this.getYearOfBirth() + ", iq=" + this.getIQLevel() + ", mother=" + motherExistName + " " + motherExistSurname + ", father=" + fatherExistName + " " + fatherExistSurname + ", " + petExist + "}";
    }
}
