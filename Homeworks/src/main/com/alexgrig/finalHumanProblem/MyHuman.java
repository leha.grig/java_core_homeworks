package com.alexgrig.finalHumanProblem;

public class MyHuman {

    public static void main(String[] args) {

    }

    private final String name;
    private final String surname;
    private MyHuman mother;
    private MyHuman father;

    private MyHuman(String name, String surname){
        this.name = name;
        this.surname = surname;
    }
    static {
        System.out.println("class MyHuman loaded");
        MyHuman unknownMother = new MyHuman("unknown", "unknown");
        MyHuman unknownFather = new MyHuman("unknown", "unknown");

    }


    public MyHuman(String name, String surname, MyHuman mother, MyHuman father){
        this.name = name;
        this.surname = surname;
        this.mother = mother;
        this.father = father;
    }
    public static MyHuman createAdam (){
        return new MyHuman ("Adam", "Adam", null, null);
    }
    public static MyHuman createEve (){
        return new MyHuman ("Eve", "Eve", null, null);
    }

}
