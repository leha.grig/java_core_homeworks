package com.alexgrig.finalHumanProblem;

import com.alexgrig.homework12.Human;

public class MyHumanCreator {
    public static void main(String[] args) {

        MyHuman adam = MyHuman.createAdam();
        MyHuman eve = MyHuman.createEve();
        MyHuman next = new MyHuman("Alex", "Grig", adam, eve);

//        MyHuman h1 = new MyHuman ("Oleg", "Oleg", unknownMother, unknownFather);
    }
}
